name = playing_around
data.outdir=../data/experiment0/output/

pipeline.runNN1a=false
pipeline.runNN1b=false
pipeline.runNN2=true
pipeline.runNN2a=false
pipeline.useComparators=true
#only algebra composer will be saved, baroni saves by default
#if true, composers will not be re-trained- it makes no sense for this to be true of CV is performed
pipeline.loadExistingComposers=false

#if true, only a few adjectives will be used
pipeline.testRun = true


composer.type = baroni

numThreads = 10

reducer.max=300
reducer.min=300
reducer.step=30


# ----------------------------- COMPARATORS -------------------------------
comp.cosine=true
comp.l1=false
comp.l2=false
comp.norm=true
comp.ent1=false
comp.ent2=false


# ----------------------------- DATA ---------------------------------------
data.file=../data/baroni.matrix.txt
data.load=true
data.baroni=true
data.numAdjNoun=25


# ---------------------------- CROSSVALIDATION -----------------------------
cv.number=1


# ---------------------------- BARONI COMPOSER -----------------------------
# app will automatically add .csv extension
baroni.nn2.outFile=../data/output/nn2/ranks
#five java threads will be spawned for each R instance

baroni.plsr_components=50


baroni.training.tempDir=../data/temp/
baroni.training.outDir=../data/temp/baroni/
baroni.training.adjNounFile=../data/composeMe.txt
