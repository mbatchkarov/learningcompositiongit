#!/bin/bash
#

# ===========================================
# Parameters for Sun Grid Engine submition
# ============================================

# Name of job
##$ -N Composers

# Shell to use
#$ -S /bin/bash

# All paths relative to current working directory
#$ -cwd

# List of queues
#$ -q parallel.q

# Define parallel environment for 12 cores
#$ -pe openmp 30

# Send mail to. (Comma separated list)
#$ -M mmb28@sussex.ac.uk

# When: [b]eginning, [e]nd, [a]borted and reschedules, [s]uspended, [n]one
#$ -m beas

# Validation level (e = reject on all problems)
#$ -w e

# Merge stdout and stderr streams: yes/no
#$ -j no


echo Traing with configuration file: $@
java -Xmx100g -cp .:target/composers-0.1-jar-with-dependencies.jar learningalgebras.Main $@