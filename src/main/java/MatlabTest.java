import org.apache.log4j.Logger;

import java.io.IOException;

/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author mmb28
 */
public class MatlabTest {

    private static final Logger log = Logger.getLogger(MatlabTest.class);

    public static void main(String[] args) {
        ProcessBuilder builder = new ProcessBuilder("matlab", "-nojvm", "-nosplash", "-r 'pslrFromDisk; exit;'");
        try {
            Process process = builder.start();
            process.waitFor();

        } catch (IOException e) {
            log.error("Could not launch MATLAB. Command: " + builder.command(), e);
        } catch (InterruptedException ex) {
            log.error(ex);
        }
    }
}
