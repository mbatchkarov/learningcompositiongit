/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;
import learningalgebras.util.Props;
import learningalgebras.util.VectorTools;
import org.apache.log4j.Logger;
import org.ujmp.core.Matrix;
import org.ujmp.core.doublematrix.impl.DefaultDenseDoubleMatrix2D;

import java.io.*;
import java.util.Arrays;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;

/**
 * @author Miroslav Batchkarov
 */
public class OneAdjectiveBaroniComposer implements Runnable, Serializable {

	private static final Logger log = Logger.getLogger(OneAdjectiveBaroniComposer.class);
	private final String baroniOutputPrefix = Props.getProps().getString("baroni.training.outDir");
	private String adjWord;
	private volatile Matrix adjMatrix;
	private transient VectorStore fvm;
	private Partition partition;
	private transient CountDownLatch latch;
	private String processName;

	public OneAdjectiveBaroniComposer(String adjWord, VectorStore fvm,
	                                  Partition p, CountDownLatch latch, String processName) {
		adjMatrix = null;
		this.adjWord = adjWord;
		this.partition = p;
		this.fvm = fvm;
		this.latch = latch;
		this.processName = processName;
	}

	public void run() {
		try {
			train(fvm, partition);
		} catch (IOException ex) {
			log.error(ex);
			ex.printStackTrace();
		} finally {
			latch.countDown();
		}

	}

	public void train(VectorStore fvm, Partition p) throws IOException {
		this.adjMatrix = null;//erase old matrix

		log.info("Training for adjective " + adjWord + ", partition " + p.getProgress());
		writeDataToFile(fvm, p);
		trainInExternalProcess(fvm);

		int n = fvm.getVectorDimensionality();
		readAdjectiveMatrixFromDisk(adjWord, n);
		log.info("End training for adjective " + adjWord);
		// ---------------------------------------------------------------------------------------------
		// --------------------------------- End PLSR --------------------------------------------------
		// ---------------------------------------------------------------------------------------------


	}

	private void trainInExternalProcess(VectorStore fvm) {
		//Create process
		String inFile = baroniOutputPrefix + adjWord + ".txt";
		String outDir = baroniOutputPrefix;// + adjWord.split("-")[0];

		int numComponents = Props.getProps().getInt("baroni.plsr_components", 50);

		log.trace("Creating external process (" + processName + ") to train adj: " + adjWord + " with components " + numComponents);
		ProcessBuilder builder = new ProcessBuilder("./src/main/shell/run" + processName + "Learning.sh", //runBaroniLearning.sh
		new File(inFile).getAbsolutePath(),
		new File(outDir).getAbsolutePath() + System.getProperty("file.separator"),
		adjWord, "" + fvm.getVectorDimensionality(),
		"" + numComponents);
		try {
			Process process = builder.start();
			int exitCode = process.waitFor();
			if (exitCode != 0) {
				process.destroy();
				log.warn("External process finished with non-zero exit code");
			}
		} catch (IOException e) {
			log.error("Could not launch external process. Command: " + builder.command(), e);
		} catch (InterruptedException ex) {
			log.error(ex);
		}
	}

	private void writeDataToFile(VectorStore fvm, Partition p) throws IOException {
		//output in the right format for baroni's R script
		File f0 = new File(baroniOutputPrefix + adjWord + ".txt");
		File f1 = new File(baroniOutputPrefix + adjWord + ".test.nouns.txt");
		BufferedWriter trainingDataWriter = new BufferedWriter(new FileWriter(f0, false));
		BufferedWriter testingDataWriter = new BufferedWriter(new FileWriter(f1, false));

		Set<String> trainingBigramStrings = new TreeSet<String>();
		for (String bigram : p.getTrainingData()) {
			final String[] split = bigram.split(" ");
			assert (split.length == 2);
			//only select the right data from the partition
			if (split[0].equals(adjWord))
				trainingBigramStrings.add(bigram);
		}

		Set<String> testingBigramStrings = new TreeSet<String>();
		Set<String> testingNounStrings = new TreeSet<String>();
		for (String bigram : p.getTestingData()) {
			final String[] split = bigram.split(" ");
			assert (split.length == 2);
			//only select the right data from the partition
			if (split[0].equals(adjWord)) {
				testingBigramStrings.add(bigram);
				testingNounStrings.add(split[1]);
			}
		}


		String msg = String.format("Training for adjective %s using %d examples, will test with %d",
		adjWord, trainingBigramStrings.size(), testingBigramStrings.size());
		log.trace(msg);

		for (String bigram : trainingBigramStrings) {
			String noun = bigram.split(" ")[1];
			double[] nounVector = fvm.getTermVector(noun);
			double[] bigramVector = fvm.getTermVector(bigram);
			if (Props.getProps().getBoolean("baroni.normalize.trainingVectors", false)) {
				nounVector = VectorTools.unitVector(nounVector);
				bigramVector = VectorTools.unitVector(bigramVector);
			}

			String nounVectorStr = Arrays.toString(nounVector);
			String bigramVectorStr = Arrays.toString(bigramVector);
//			String s = bigram + " " + nounVectorStr + " " + bigramVectorStr + "\n";
			String s = bigram + "\t" + nounVectorStr + "\t" + bigramVectorStr + "\n";
//			String s = bigram.replace(" ", "_") + " " + nounVectorStr + " " + bigramVectorStr + "\n";
			s = s.replace("[", "");
			s = s.replace("]", "");
			s = s.replace(",", "");
			s = s.replace(" ", "\t");//remove extra stuff introduced by Arrays.toString()
			trainingDataWriter.write(s);

			String s1 = noun + "\t" + nounVectorStr + "\n";
			s1 = s1.replace("[", "");
			s1 = s1.replace("]", "");
			s1 = s1.replace(",", "");
			s1 = s1.replace(" ", "\t");//remove extra stuff introduced by Arrays.toString()
			testingDataWriter.write(s1);
		}
		trainingDataWriter.close();
		testingDataWriter.close();

	}

	public Matrix getAdjMatrix() {
		return adjMatrix;
	}

	protected double[] composeStringAndVector(String word1, double[] word2) {
		double[] vectorToPassToR = Arrays.copyOf(word2, word2.length);
		if (Props.getProps().getBoolean("baroni.normalize.testingVectors", false)) {
			vectorToPassToR = VectorTools.unitVector(vectorToPassToR);
		}

		double[] result;

		//=============================== begin matlab code ======================================
		int n = word2.length;
		if (this.adjMatrix == null) {
			if (!readAdjectiveMatrixFromDisk(word1, n)) return null;
		}//adjective matrix now available

		//R- just load matrix
//		Matrix nounMatrix = new DefaultDenseDoubleMatrix2D(word2, 1, word2.length);

		//matlab- prepend a column of 1s to noun matrix
		double[] nounVector = new double[word2.length];
//		double[] nounVector = new double[word2.length + 1];//TODO matrix WITHOUT intercept
		System.arraycopy(word2, 0, nounVector, 0, word2.length);
//		nounVector[0] = 1d;//TODO matrix WITHOUT intercept
		Matrix nounMatrix = new DefaultDenseDoubleMatrix2D(nounVector, 1, nounVector.length);

		Matrix res = nounMatrix.mtimes(adjMatrix);
		result = res.toDoubleArray()[0];
		if (result == null) {
			return null;//indicates R failure, for whatever reason
		} else if (Props.getProps().getBoolean("baroni.normalize.rResult", false)) {
			result = VectorTools.unitVector(result);
		}
		return result;
	}

	/**
	 * Adjective to find matrix for and its expected dimensionality
	 *
	 * @param adjString includes "-j" at the end
	 * @param n
	 * @return
	 */
	private boolean readAdjectiveMatrixFromDisk(String adjString, int n) {
		String prefix = Props.getProps().getString("baroni.training.outDir");
		log.trace("Loading adjective matrix (.betas) for " + adjString + " from " + prefix);
		String[] adjFileNames = new File(prefix).list();
		Arrays.sort(adjFileNames);//required for binary search
		int pos = Arrays.binarySearch(adjFileNames, adjString + "betas.txt");
		if (pos < 0) {
			log.error("Could not find matrix for adjective " + adjString + ", skipping!");
			return false;
		}

		File adjFile = new File(prefix + adjFileNames[pos]);
		int lineNumber = 0;
		adjMatrix = new DefaultDenseDoubleMatrix2D(n, n);//R //TODO matrix WITHOUT intercept
//		adjMatrix = new DefaultDenseDoubleMatrix2D(n + 1, n);//matlab- matrix has an extra row

		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new FileReader(adjFile));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] values = line.split("\t");
				for (int i = 0; i < values.length; i++) {
					adjMatrix.setAsDouble(Double.parseDouble(values[i]), lineNumber, i);
				}
				lineNumber++;
			}
		} catch (IOException ex) {
			log.error(ex);
			return false;
		}
//		log.info(String.format("Matrix for adjective %s is:\n %s", adjString, adjMatrix));
		return true;
	}
}
