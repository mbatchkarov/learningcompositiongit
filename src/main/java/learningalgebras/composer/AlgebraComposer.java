/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;

/**
 * @author DaoCla
 */
public class AlgebraComposer  implements  BigramComposer {

	private AlgebraProduct product;

	@Override
	public void train(VectorStore fvm, Partition p) {
		product = new AlgebraProduct(fvm, p);
	}

	private double[] composeVectors(double[] word1, double[] word2) {
		return product.product(word1, word2);
	}

	public double[] compose(VectorStore vs, String word1, String word2) {
		return composeVectors(vs.getTermVector(word1), vs.getTermVector(word2));
	}

	@Override
	public String toString() {
		return "AlgebraComposer{" +
		"product=" + product +
		'}';
	}
}
