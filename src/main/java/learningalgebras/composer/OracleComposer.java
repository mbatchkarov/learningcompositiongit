package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;

/**
 * Returns the observed vector for a bigram
 *
 * User: mmb28
 * Date: 14/01/2013
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
public class OracleComposer implements BigramComposer {

	private VectorStore store;

	@Override
	public double[] compose(VectorStore store, String word1, String word2) {
		return store.getPairVector(word1, word2);
	}

	@Override
	public void train(VectorStore fvm, Partition p) {
		//no training needed
	}

	@Override
	public String toString() {
		return "OracleComposer";
	}

}
