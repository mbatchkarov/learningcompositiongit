/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.FeatureVectorMatrix;
import learningalgebras.Partition;
import learningalgebras.VectorStore;
import learningalgebras.util.KroneckerProduct;
import learningalgebras.util.MyThreadFactory;
import learningalgebras.util.Props;
import org.apache.log4j.Logger;
import org.ojalgo.array.ArrayUtils;
import org.ojalgo.matrix.decomposition.SingularValue;
import org.ojalgo.matrix.decomposition.SingularValueDecomposition;
import org.ojalgo.matrix.store.MatrixStore;
import org.ojalgo.matrix.store.PhysicalStore;
import org.ojalgo.matrix.store.PrimitiveDenseStore;

import java.io.Serializable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Class for computing the product between two or more vectors. The constructor
 * for this class will learn the parameters for the product.
 *
 * @author bc73
 */
public class AlgebraProduct implements Serializable {

	private static final Logger log = Logger.getLogger(AlgebraProduct.class);
	private final int n;
	private final double[][] components;

	/**
	 * Creates a new product.
	 *
	 * @param fvm the feature vector matrix to learn the product for
	 * @param p
	 * @throws IllegalArgumentException if the number of pairs in the matrix
	 *                                  is less than the square of the number of components in each vector
	 */
	public AlgebraProduct(final VectorStore fvm, final Partition p) {
		n = fvm.getVectorDimensionality();
		components = new double[n][n * n];
		if (n * n > fvm.getPairs().size()) {
			String msg = "Number of pairs must be >= vector components ^ 2";
			throw new IllegalArgumentException(msg);
		}
		PhysicalStore<Double> x = buildX(fvm);
//		log.trace("Matrix Y-" + x);
		SingularValue<Double> qr = SingularValueDecomposition.makePrimitive();
		log.info("Performing SVD of X");
		boolean result = qr.compute(x);
		assert (result);
		assert (qr.isSolvable());
		MatrixStore<Double> inverse = qr.getInverse();

		log.info("Computing Kronecker product");
		//final PhysicalStore inverseSquared = kroneckerProduct(inverse, inverse);
		final KroneckerProduct inverseSquared = new KroneckerProduct(inverse, inverse);

		log.info("Computing product");
		final int processors = Props.getProps().getInt("numThreads", 4); //Runtime.getRuntime().availableProcessors();
		ExecutorService threadPool = Executors.newFixedThreadPool(processors,
		new MyThreadFactory("kronProduct", null, Thread.MAX_PRIORITY));
		for (int dim = 0; dim < n; dim++) {
			final int t = dim;
			threadPool.execute(new Runnable() {

				public void run() {
					try {
						PhysicalStore<Double> y = buildY(fvm, t, p);
//						log.trace("Matrix Y-" + t + " : " + y);
						//JamaMatrix y_matrix = new JamaMatrix(y);
//						log.debug("Multiplying dimension " + t);
						double[] beta = ArrayUtils.toRawCopyOf(
						inverseSquared.multiplyRight(y).transpose())[0];
						//double[] beta = ArrayUtils.toRawCopyOf(x_matrix.solve(y_matrix).transpose())[0];
						assert beta.length == n * n;
						components[t] = beta;
//						log.debug("Done Multiplying dimension " + t);
					} catch (Exception ex) {
						ex.printStackTrace();
						log.error(ex);
					}
				}
			});
		}
		try {
			threadPool.shutdown();
			boolean threadResult = threadPool.awaitTermination(1, TimeUnit.DAYS);
			assert threadResult;
		} catch (InterruptedException e) {
			log.error("Exception constructing product: " + e.toString());
		}
		log.info("Product computed");
	}

	/**
	 * Builds the X matrix. The matrix has a dimensionality of <var>words</var>
	 * *<var>n</var>, where <var>n</var> is the number of
	 * components in a word vector.
	 *
	 * @return the X matrix.
	 */
	private PhysicalStore<Double> buildX(VectorStore fvm) {
//        final int numTerms = fvm.getTerms().size();
		final int numTerms = fvm.getTerms().size();//TODO does intended def of terms here include bigrams?
		final int rows = numTerms;
		final int cols = n;
		log.info(String.format("Building %dx%d X matrix", rows, cols));
		PhysicalStore<Double> x = PrimitiveDenseStore.FACTORY.makeEmpty(rows, cols);
		int row = 0;
		for (String iTerm : fvm.getTerms()) {//TODO does intended def of terms here include bigrams?
			double[] iVector = fvm.getTermVector(iTerm);
			int col = 0;
			for (double rComponent : iVector) {
				x.set(row, col, rComponent);
				col++;
			}
			assert cols == col : "X contains empty columns";
			row++;
		}
		assert rows == row : "X contains empty rows";
		return x;

//        Object[] arr = fvm.getTerms().toArray();
//        for (int i = 0; i < arr.length; i++) {
//            double[] iVector = fvm.getTermVector((String) arr[i]);
//            System.out.println("vector len is " + iVector.length);
//            for (int j = 0; j < iVector.length; j++) {
//                double d = iVector[j];
//                System.out.println(i +","+j);
//                x.set(i, j, d);
//            }
//        }
//        return x;
	}

	private PhysicalStore<Double> kroneckerProduct(MatrixStore<Double> A,
	                                               MatrixStore<Double> B) {
		int aRows = A.getRowDim();
		int aColumns = A.getColDim();
		int bRows = B.getRowDim();
		int bColumns = B.getColDim();
		PhysicalStore<Double> result =
		PrimitiveDenseStore.FACTORY.makeEmpty(aRows * bRows, aColumns * bColumns);
		for (int i = 0; i < aColumns; ++i) {
			for (int j = 0; j < aRows; ++j) {
				for (int k = 0; k < bColumns; ++k) {
					for (int l = 0; l < bRows; ++l) {
						result.set(j * aRows + l, i * aColumns + k,
						A.get(j, i) * B.get(l, k));
					}
				}
			}
		}
		return result;
	}

//
//    /**
//     * Builds the X matrix. The matrix has a dimensionality of <var>word
//     * pairs</var> * <var>n</var>^2, where <var>n</var> is the number of
//     * components in a word vector.
//     *
//     * @return the X matrix.
//     */
//    private PhysicalStore<Double> buildX(FeatureVectorMatrix fvm) {
//        //final int rows = fvm.getPairs().size();
//        final int numTerms = fvm.getTerms().size();
//        final int rows = numTerms*numTerms;
//        final int cols = n * n;
//        log.info(String.format("Building %dx%d X matrix", rows, cols));
//        PhysicalStore<Double> x = PrimitiveDenseStore.FACTORY.makeEmpty(rows, cols);
//        int row = 0;
//        for (String iTerm : fvm.getTerms()) {
//            for (String jTerm : fvm.getTerms()) {
//                //if (fvm.containsTermPairVector(iTerm, jTerm)) {
//                    double[] iVector = fvm.getTermVector(iTerm);
//                    double[] jVector = fvm.getTermVector(jTerm);
//                    int col = 0;
//                    for (double rComponent : iVector) {
//                        for (double sComponent : jVector) {
//                            x.set(row, col++, rComponent * sComponent);
//                        }
//                    }
//                    assert cols == col : "X contains empty columns";
//                    row++;
//                //}
//            }
//        }
//        assert rows == row : "X contains empty rows";
//        return x;
//    }

	/**
	 * Returns the <var>t</var>th column vector of word pair values.
	 *
	 * @param t column index
	 * @return vector of the <var>t</var>th column of word pair values.
	 */
	private PhysicalStore<Double> buildY(VectorStore fvm, int t, Partition part) {
		log.info("Building Y-" + t);
		//final int rows = fvm.getPairs().size();
		final int numTerms = fvm.getTerms().size();//TODO does intended def of terms here include bigrams?
		final int rows = numTerms * numTerms;
		PhysicalStore<Double> y = PrimitiveDenseStore.FACTORY.makeEmpty(rows, 1);
		int row = 0;
		for (String iTerm : fvm.getTerms()) {//TODO does intended def of terms here include bigrams?
			for (String jTerm : fvm.getTerms()) {//TODO does intended def of terms here include bigrams?
				double value = 0.0;
				if (fvm.containsTermPairVector(iTerm, jTerm) && (part.getTrainingData().contains(iTerm + " " + jTerm))) {
					value = fvm.getPairVector(iTerm, jTerm)[t];
				}
				y.set(row++, 0, value);
			}
		}
		log.info("Done building Y-" + t);
		if (rows != row) throw new IllegalStateException("y contains empty rows");
		return y;
	}

	/**
	 * Returns a component from the product vector.
	 *
	 * @param r <var>r</var> value
	 * @param s <var>s</var> value
	 * @param t <var>t</var> value
	 * @return the corresponding component from the product vector
	 */
	public double getComponent(int r, int s, int t) {
		return components[t][r * n + s];
	}

	/**
	 * Computes the product for two term vectors.
	 *
	 * @param term1 first term vector
	 * @param term2 second term vector
	 * @return the product for the two word vectors
	 * @throws IllegalArgumentException if the length of i or j != n
	 */
	public double[] product(double[] term1, double[] term2) {
		if (term1.length != n || term2.length != n) {
			throw new IllegalArgumentException("dimension mismatch");
		}
		double[] product = new double[n];
		for (int t = 0; t < n; t++) {
			double component = 0;
			for (int r = 0; r < n; r++) {
				for (int s = 0; s < n; s++) {
					component += term1[r] * term2[s] * getComponent(r, s, t);
				}
			}
			product[t] = component;
		}
		return product;
	}

	/**
	 * Computes the product for an arbitrary length phrase.
	 *
	 * @param terms the terms in the phrase
	 * @return the product of the terms
	 */
	public double[] product(FeatureVectorMatrix fvm, String... terms) {
		double[] product = fvm.getTermVector(terms[0]);
		for (int i = 1; i < terms.length; i++) {
			double[] ithTerm = fvm.getTermVector(terms[i]);
			product = product(product, ithTerm);
		}
		return product;
	}

	@Override
	public String toString() {
		String r = "";
		for (int i = 0; i < n; ++i) {
			for (int j = 0; j < n; ++j) {
				for (int k = 0; k < n; ++k) {
					r += Double.toString(getComponent(i, j, k)) + " ";
				}
				r += "\n";
			}
			r += "\n";
		}

		return r;
	}


}
