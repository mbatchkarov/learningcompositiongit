/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;

import java.io.Serializable;

public interface BigramComposer extends Serializable {

	public double[] compose(VectorStore store, String word1, String word2);

	public void train(VectorStore fvm, Partition p);

}
