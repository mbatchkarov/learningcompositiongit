/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;
import learningalgebras.util.Props;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeSet;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NewBaroniComposer implements BigramComposer {

	private static final Logger log = Logger.getLogger(BigramComposer.class);
	private static final long serialVersionUID = 1424325L;
	private Map<String, OneAdjectiveBaroniComposer> composers;
	private transient ExecutorService exec;//for training sub-composers
	private String processName;

	public NewBaroniComposer() {
		this.composers = new HashMap<String, OneAdjectiveBaroniComposer>();
		this.processName = "Baroni";
	}

	/**
	 * Sets the program which is used to perform PLSR
	 * @param r if true, the R script provided by Marco Baroni is used, else my own Matlab code
	 */
	public void setLearningInR(boolean r){
		if(r){
			this.processName = "Baroni";
		}
		else{
			this.processName = "Matlab";
		}
	}

	/**
	 * Composes according to Baroni (2010) methodology
	 *
	 * @param store where to look for vectors
	 * @param word1 adjective
	 * @param word2 nouns
	 * @return vector representing the noun phrase
	 * @throws IllegalArgumentException if composition failed. This could happen if the composers does
	 *                                  not have a representation (matrix) for the adjective, because the adj was not in the training set
	 */
	@Override
	public double[] compose(VectorStore store, String word1, String word2) {
		if (composers.containsKey(word1))
			return composers.get(word1).composeStringAndVector(word1, store.getTermVector(word2));
		else {
			final String message = "Don't know how to compose " + word1 + " " + word2;
			log.warn(message);
			throw new IllegalArgumentException(message);
		}
	}

	@Override
	public void train(VectorStore fvm, Partition p) {
		exec = Executors.newFixedThreadPool(Props.getProps().getInt("numThreads", 4));
		log.info("Training Baroni composer in " + Thread.currentThread());
		clearTempFiles();


		TreeSet<String> adjsToTrainFor = new TreeSet<String>();
		for (String bigram : p.getTrainingData()) {
			adjsToTrainFor.add(bigram.split(" ")[0]);
		}

		int matricesToLearn = adjsToTrainFor.size();
		CountDownLatch latch = new CountDownLatch(matricesToLearn);
		for (String adjWord : adjsToTrainFor){
			//prep the object that does the actual training for this adjective
			OneAdjectiveBaroniComposer c = new OneAdjectiveBaroniComposer(adjWord, fvm, p, latch, processName);
			composers.put(adjWord, c);//keep reference to trained sub-composer for later use--it is not ready yet!
			exec.submit(c);//start training
		}
		exec.shutdown();

		try {
			latch.await();//wait for all sub-composers to finish
		} catch (InterruptedException e) {
			log.error(e);
		}
		log.info("Finished training baroni composer, fold " + p.getProgress());
	}

	/**
	 * Deletes all temporary files that constitute the internal state of this composer
	 */
	private void clearTempFiles() {
		String prefix = Props.getProps().getString("baroni.training.tempDir");
		String[] dirs = new File(prefix).list();
		for (int j = 0; j < dirs.length; j++) {

			File f = new File(prefix + dirs[j]);
			if (!f.isDirectory()) {
				continue;//NPE because of .DS_Store file on OS X
			}
			File[] adjFiles = (f.listFiles());
			for (int i = 0; i < adjFiles.length; i++) {
				File f1 = adjFiles[i];
				if (f1.exists()) {
					f1.delete();
				}
			}
		}
	}

	@Override
	public String toString() {
		return "NewBaroniComposer";
	}
}
