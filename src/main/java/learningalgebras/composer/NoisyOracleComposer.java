package learningalgebras.composer;

import com.rits.cloning.Cloner;
import learningalgebras.Partition;
import learningalgebras.VectorStore;

/**
 * Returns the observed bigram vector, modifying it a little bit byt adding `range` to its first component
 */
public class NoisyOracleComposer implements BigramComposer {

	private Cloner c;
	private double range;

	public NoisyOracleComposer(double diff) {
		this.c = new Cloner();
		this.range = diff;
	}

	@Override
	public double[] compose(VectorStore store, String word1, String word2) {
		double[] adjVector = c.deepClone(store.getPairVector(word1, word2));
		adjVector[0] += range;
		return adjVector;
	}

	@Override
	public void train(VectorStore fvm, Partition p) {
		//no training needed
	}

	@Override
	public String toString() {
		return "NoisyOracleComposer";
	}
}
