/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;

/**
 * Composes that ignores the second word
 *
 * @author DaoCla
 */
public class IgnoreSecondComposer  implements  BigramComposer {
	@Override
	public double[] compose(VectorStore store, String word1, String word2) {
		return store.getTermVector(word1);
	}

	@Override
	public void train(VectorStore fvm, Partition p) {
		//no training required
	}
}
