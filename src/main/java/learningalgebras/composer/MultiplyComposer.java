/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.composer;

import learningalgebras.Partition;
import learningalgebras.VectorStore;
import learningalgebras.util.VectorTools;

/**
 * Composes using pointwise multiplication
 * @author DaoCla
 */
public class MultiplyComposer implements BigramComposer {
	@Override
	public double[] compose(VectorStore store, String word1, String word2) {
		return VectorTools.mul(store.getTermVector(word1), store.getTermVector(word2));
	}

	@Override
	public void train(VectorStore fvm, Partition p) {
		//no training required
	}

}
