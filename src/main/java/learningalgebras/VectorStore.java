/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras;

import java.util.Set;

/**
 * An interface for classes that provide observed vectors
 */
public interface VectorStore {

	public double[] getPairVector(String term1, String term2);

	public boolean containsTermPairVector(String term1, String term2);

	public double[] getTermVector(String term);

	public int getVectorDimensionality();

	/**
	 * Returns the set of adjectives in the matrix.
	 *
	 * @return set of adjectives
	 */
	public Set<String> getAdjs() ;

	/**
	 * Returns the set of nouns in the matrix.
	 *
	 * @return set of nouns
	 */
	public Set<String> getNouns() ;

	/**
	 * Returns the set of terms in the matrix, ie the union of the set of
	 * adjectives and nouns.
	 *
	 * @return set of terms
	 */
	public Set<String> getTerms() ;

	/**
	 * Returns the all terms for which a feature vector exists
	 *
	 * @return set of terms
	 */
	public Set<String> getAllTerms() ;

	/**
	 * Returns the set of term pairs in the matrix.
	 *
	 * @return set of term pairs
	 */
	public Set<String> getPairs();
}
