/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras;

import learningalgebras.composer.AlgebraProduct;
import learningalgebras.eval.similarity.Similarity;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.*;

/**
 * The thesaurus works by computing the products for all phrase permutations
 * in a learned product and stores the phrase vectors in a map. Similar phrases
 * are created by computing the cosine similarity between an specified phrase
 * and all other phrases in the thesaurus.
 *
 * @author bc73
 */
public class Thesaurus implements Serializable {

    private static final Logger log = Logger.getLogger(Thesaurus.class);
    private final ConcurrentMap<String, double[]> computedVectors;
    private final Set<String> adjSet;
    private final Set<String> nounSet;

    /**
     * Creates a new thesaurus.
     *
     * @param fvm the feature vector matrix containing the term vectors
     * @param f the learned product
     * @param phraseLength the maximum phrase length to include in the thesaurus
     */
    public Thesaurus(FeatureVectorMatrix fvm, AlgebraProduct f, int phraseLength) {
        computedVectors = new ConcurrentHashMap<String, double[]>();
        adjSet = new HashSet<String>(fvm.getAdjs());
        nounSet = new HashSet<String>(fvm.getNouns());
        int processors = Runtime.getRuntime().availableProcessors();
        ExecutorService threadPool = Executors.newFixedThreadPool(processors);
        log.info("Computing all permutations of phrases up to a word length of "
                + phraseLength);
        for (int n = 1; n <= phraseLength; n++) {
            populateVectorMap(threadPool, fvm, f, new String[n], 0);
        }
        threadPool.shutdown();
        try {
            boolean terminated = threadPool.awaitTermination(1, TimeUnit.DAYS);
            if (!terminated) {
                log.error("Timed out waiting for thread pool to terminate");
            }
        } catch (InterruptedException e) {
            threadPool.shutdownNow();
        }
        log.info("Thesaurus contains " + computedVectors.size() + " phrases");
    }

    /**
     * Computes the product for all phrases of specified length and stores
     * the result in the context vector map
     *
     * @param exec executor used for parallelisation
     * @param fvm the feature vector matrix containing the terms vectors
     * @param f the learned product
     * @param phrase an empty array of strings whose length is equal to the
     * length of the phrase the product should be compute for
     * @param i counter used to keep track of the recursion depth (this should
     * always be set to 0 when invoking the method for the first time)
     */
    private void populateVectorMap(Executor exec, final FeatureVectorMatrix fvm,
            final AlgebraProduct f, String[] phrase, int i) {
        if (i < phrase.length - 1) {
            for (String adj : adjSet) {
                phrase[i] = adj;
                populateVectorMap(exec, fvm, f, phrase, i + 1);
            }
        } else {
            final String[] copy = phrase.clone();
            exec.execute(new Runnable() {

                public void run() {
                    for (String noun : nounSet) {
                        copy[copy.length - 1] = noun;
                        double[] product = f.product(fvm, copy);
                        computedVectors.put(join(copy), product);
                    }
                }
            });
        }
    }

    /**
     * Computes similar phrases using the specified similarity comparator.
     *
     * @param phrase the phrase to use
     * @params comparator the similarity comparator to use
     * @return a map of phrases and cosine similarities sorted by descending
     * similarity
     */
    public Map<String, Double> getSimilar(String phrase, Similarity comparator) {
        double[] p = computedVectors.get(phrase);
        if (p == null) {
            String format = "Phrase '%s' is not in the thesaurus";
            throw new IllegalArgumentException(String.format(format, phrase));
        }
        Map<String, Double> results = new HashMap<String, Double>();
        for (Map.Entry<String, double[]> vector : computedVectors.entrySet()) {
            double cos = comparator.getSimilarity(p, vector.getValue());
            results.put(vector.getKey(), cos);
        }
        return results;
    }

    /**
     * Concatenates an array of strings into a single space separated string.
     *
     * @param strings the array of strings
     * @return the concatenated string
     */
    private static String join(String[] strings) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < strings.length - 1; i++) {
            sb.append(strings[i]);
            sb.append(' ');
        }
        sb.append(strings[strings.length - 1]);
        return sb.toString();
    }

    /**
     * Returns the set of adjectives used in the thesaurus.
     *
     * @return set of adjectives
     */
    public Set<String> getAdjSet() {
        return adjSet;
    }

    /**
     * Returns the set of nouns used in the thesaurus.
     *
     * @return set of nouns
     */
    public Set<String> getNounSet() {
        return nounSet;
    }

    /**
     * Returns the number of phrases in the thesaurus.
     *
     * @return number of phrases
     */
    public int getPhraseCount() {
        return computedVectors.size();
    }
}
