/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras;

import learningalgebras.util.WordTally;
import org.apache.log4j.Logger;
import org.ujmp.core.Matrix;
import org.ujmp.core.matrix.SparseMatrix;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Term/context matrix. Terms (ie nouns/adjectives and pairs of 
 * nouns/adjectives) are represented by the rows and the contexts they appear
 * in (ie verbs) are represented by columns. The matrix has three maps that are
 * used to identify the row/colum indexes for the terms/contexts.
 *
 * @author bc73
 */
public class TermContextMatrix implements Serializable {

    private static final Logger log = Logger.getLogger(TermContextMatrix.class);
    private Matrix m;
    private SortedSet<String> adjSet, nounSet;
    private Map<String, Integer> termMap, contextMap;
    private transient Set<String> verbContextSet, nounContextSet;
    private HashMap<String, double[]> featureVectors;

    /**
     * Creates a term/context matrix from a file. The input file must contain
     * one phrase per line. The first word in the phrase must be verb, the last
     * word must be a noun and all other words must be adjectives. Phrases must
     * contain at least one adjective, but they may contain more.
     *
     * @param adjLimit the maximum number of adjectives to include in the matrix
     * @param nounLimit the maximum number of nouns to include in the matrix
     * @param verbContextLimit the maximum number of verbs to use as context
     * features for nouns
     * @param nounContextLimit the maximum number of nouns to use as context
     * features for adjectives (set to 0 if verbs should be used as the features
     * of adjectives)
     * @param numOmittedAdjectives how many of the most frequent adjectives to ignore
     * @return a populated term/context matrix
     */
    public TermContextMatrix(String phraseFilePath, int adjLimit, int nounLimit,
                             int verbContextLimit, int nounContextLimit, int numOmittedAdjectives) throws IOException {
        populateAdjNounSets(phraseFilePath, adjLimit, nounLimit, numOmittedAdjectives);
        populateContextSets(phraseFilePath, verbContextLimit, nounContextLimit);
        populateTermMap(phraseFilePath);
        populateContextMap();
        buildMatrix(phraseFilePath);

        for (int i = 0; i < m.getRowCount(); i++) {
            double[] vector = new double[(int)m.getColumnCount()];
            for (int j = 0; j < vector.length; j++) {
                vector[j] = m.getAsInt(i,j);

            }
        }
    }

    /**
     * Creates a new term/context matrix.
     *
     * @param m a matrix of term/context frequencies
     * @param adjMap map of term names and the row indices
     * @param termPairMap map of pairs of term names and row indices
     * @param verbMap map of context names and the columns indices
     */
//    public TermContextMatrix(Matrix m, Set<String> adjSet, Set<String> nounSet,
//            Map<String, Integer> termMap, Map<String, Integer> contextMap) {
//        this.m = m;
//        this.adjSet = adjSet;
//        this.nounSet = nounSet;
//        this.termMap = termMap;
//        this.contextMap = contextMap;
//    }

    /**
     * Returns the frequency matrix.
     *
     * @return the frequency matrix
     */
    public Matrix getMatrix() {
        return m;
    }

    /**
     * Returns a mapping of terms to row indices.
     *
     * @return term index map
     */
    public Map<String, Integer> getTermMap() {
        return termMap;
    }

    /**
     * Returns a mapping of contexts to column indices.
     *
     * @return context index map
     */
    public Map<String, Integer> getContextMap() {
        return contextMap;
    }

    /**
     * Returns the set of adjectives in the matrix.
     *
     * @return set of adjectives
     */
    public Set<String> getAdjSet() {
        return adjSet;
    }

    /**
     * Returns the set of nouns (used as terms) in the matrix.
     *
     * @return set of terms
     */
    public Set<String> getNounSet() {
        return nounSet;
    }

    /**
     * Populates the set of adjectives and nouns that will be included the
     * in the term context matrix. This is achieved by creating a tally for each
     * adjective and noun found in the phrase input and selecting the most
     * popular ones.
     *
     * @param pathname path to the phrase file
     * @param adjLimit the maximum number of adjectives to include
     * @param nounLimit the maximum number of nouns to include
     *
     * @param numOmittedAdjectives
     * @throws IOException on I/O error
     */
    private void populateAdjNounSets(String pathname, int adjLimit,
                                     int nounLimit, int numOmittedAdjectives) throws IOException {
        log.info("Using an adjective limit of " + adjLimit);
        log.info("Using a noun limit of " + nounLimit);
        WordTally nounTally = new WordTally();
        WordTally adjTally = new WordTally();
        BufferedReader reader = new BufferedReader(new FileReader(pathname));
        String line;
        while ((line = reader.readLine()) != null) {
	        System.out.println(line);
            String[] phrase = line.split(" ");
            int nounIndex = phrase.length - 1;
            for (int i = 0; i < nounIndex; i++) {
                adjTally.increment(phrase[i]);
            }
            nounTally.increment(phrase[nounIndex]);
        }
        reader.close();
        adjSet = adjTally.getMostPopular(0, adjLimit);
        log.info(String.format("Including %d adjectives in the matrix",
                adjSet.size()));
        nounSet = nounTally.getMostPopular(numOmittedAdjectives, nounLimit);
        log.info(String.format("Including %d nouns in the matrix",
                nounSet.size()));
    }

    /**
     * Populates the set of verbs and nouns that will be used as context
     * features of the chosen adjectives and nouns. Contexts that only occur
     * once are ignored as a context needs to occur with more than one thing
     * for it to be useful.
     *
     * @param pathname path to the phrase file
     * @param verbContextLimit the maximum number of verbs to use as context
     * features for nouns
     * @param nounContextLimit the maximum number of nouns to use as context
     * features for adjectives (set to 0 if verbs should be used as the features
     * of adjectives)
     * @throws IOException on I/O error
     */
    private void populateContextSets(String pathname, int verbContextLimit,
            int nounContextLimit) throws IOException {
        log.info("Using a verb context limit of " + verbContextLimit);
        log.info("Using a noun context limit of " + nounContextLimit);
        WordTally verbTally = new WordTally();
        WordTally nounTally = new WordTally();
        BufferedReader reader = new BufferedReader(new FileReader(pathname));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] phrase = line.split(" ");
            String noun = phrase[phrase.length - 1];
            if (nounSet.contains(noun)) {
                String verb = phrase[0];
                verbTally.increment(verb);
            }
            for (int i = 1; i < phrase.length - 1; i++) {
                String adj = phrase[i];
                if (adjSet.contains(adj)) {
                    nounTally.increment(noun);
                }
            }
        }
        reader.close();
        verbContextSet = verbTally.getAboveThreshold(verbContextLimit, 2);
        log.info(String.format("Using %d verbs as the context for nouns",
                verbContextSet.size()));
        nounContextSet = nounTally.getAboveThreshold(nounContextLimit, 2);
        log.info(String.format("Using %d nouns as the context for adjectives",
                nounContextSet.size()));
    }

    /**
     * Populates the maps that will be used to identify which row in the matrix
     * corresponds to a particular adjective, noun or pair of adjectives/nouns.
     *
     * @param pathname path to the phrase file
     * @throws IOException on I/O error
     */
    private void populateTermMap(String pathname) throws IOException {
        termMap = new HashMap<String, Integer>();
        int row = 0;
        for (String adj : adjSet) {
            termMap.put(adj, row++);
        }
        for (String noun : nounSet) {
            if (!termMap.containsKey(noun)) {
                termMap.put(noun, row++);
            }
        }
        BufferedReader reader = new BufferedReader(new FileReader(pathname));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] phrase = line.split(" ");
            String verb = phrase[0];
            String noun = phrase[phrase.length - 1];
            if (nounSet.contains(noun) && verbContextSet.contains(verb)) {
                String adj = phrase[phrase.length - 2];
                String adjNoun = adj + " " + noun;
                if (adjSet.contains(adj) && !termMap.containsKey(adjNoun)) {
                    termMap.put(adjNoun, row++);
                }
            }
            //todo uncomment here to enable adj-adj phrases
//            for (int i = 1; i < phrase.length - 2; i++) {
//                String adj1 = phrase[i];
//                String adj2 = phrase[i + 1];
//                if (adjSet.contains(adj1) && adjSet.contains(adj2)
//                        && (nounContextSet.contains(noun)
//                        || (nounContextSet.isEmpty()
//                        && verbContextSet.contains(verb)))) {
//                    String adjAdj = adj1 + " " + adj2;
//                        if (!termMap.containsKey(adjAdj)) {
//                            termMap.put(adjAdj, row++);
//                        }
//                }
//            }
        }
        reader.close();
        Set<String> termSet = new HashSet<String>(adjSet);
        termSet.addAll(nounSet);
        log.info(String.format("Including %d term pairs in the matrix",
                termMap.size() - termSet.size()));
    }

    /**
     * Populates the maps that will be used to identify which column in the
     * matrix corresponds to a particular verb/noun context.
     */
    private void populateContextMap() {
        contextMap = new HashMap<String, Integer>();
        int col = 0;
        for (String verb : verbContextSet) {
            if (contextMap.put(verb, col) == null) {
                col++;
            }
        }
        for (String noun : nounContextSet) {
            if (contextMap.put(noun, col) == null) {
                col++;
            }
        }
    }

    /**
     * Builds the matrix. Rows correspond with terms and columns with contexts.
     * Each entry in the matrix is the number of times a terms occurs with a
     * particular context.
     * 
     * @param pathname path to the phrase file
     * @return a populated matrix
     * @throws IOException on I/O error
     */
    private void buildMatrix(String pathname) throws IOException {
        final int rows = termMap.size();
        final int cols = contextMap.size();
        log.info(String.format("Building %dx%d matrix", rows, cols));
        m = SparseMatrix.factory.zeros(rows, cols);
        BufferedReader reader = new BufferedReader(new FileReader(pathname));
        String line;
        while ((line = reader.readLine()) != null) {
            String[] phrase = line.split(" ");
            String verb = phrase[0];
            String noun = phrase[phrase.length - 1];
            // noun
            if (nounSet.contains(noun) && verbContextSet.contains(verb)) {
                int col = contextMap.get(verb);
                assert col < cols;
                int row = termMap.get(noun);
                assert row < rows;
                m.setAsInt(m.getAsInt(row, col) + 1, row, col);
                // adj noun
                String adj = phrase[phrase.length - 2];
                if (adjSet.contains(adj)) {
                    String adjNoun = adj + " " + noun;
                    row = termMap.get(adjNoun);
                    assert row < rows;
                    m.setAsInt(m.getAsInt(row, col) + 1, row, col);
                }
            }
            // adj
            int col = -1;
            if (nounContextSet.contains(noun)) {
                col = contextMap.get(noun);
            } else if (nounContextSet.isEmpty()
                    && verbContextSet.contains(verb)) {
                col = contextMap.get(verb);
            }
            if (col != -1) {
                assert col < cols;
                for (int i = 1; i < phrase.length - 1; i++) {
                    String adj = phrase[i];
                    if (adjSet.contains(adj)) {
                        int row = termMap.get(adj);
                        assert row < rows;
                        m.setAsInt(m.getAsInt(row, col) + 1, row, col);
                        // adj adj
//                        if (i < phrase.length - 2) {
//                            String adj2 = phrase[i + 1];
//                            if (adjSet.contains(adj2)) {
//                                String adjAdj = adj + " " + adj2;
//                                row = termMap.get(adjAdj);
//                                assert row < rows;
//                                m.setAsInt(m.getAsInt(row, col) + 1, row, col);
//                            }
//                        }
                    }
                }
            }
        }
        reader.close();
    }

    /**
     * @return the featureVectors
     */
    public HashMap<String, double[]> getFeatureVectors() {
        return featureVectors;
    }
}
