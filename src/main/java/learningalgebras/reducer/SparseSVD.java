/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.reducer;

import ch.akuhn.edu.mit.tedlab.DMat;
import ch.akuhn.edu.mit.tedlab.SMat;
import ch.akuhn.edu.mit.tedlab.SVDRec;
import ch.akuhn.edu.mit.tedlab.Svdlib;
import org.apache.log4j.Logger;
import org.ujmp.core.Matrix;
import org.ujmp.core.MatrixFactory;
import org.ujmp.core.calculation.Calculation.Ret;
import org.ujmp.core.matrix.DenseMatrix;
import org.ujmp.core.matrix.SparseMatrix;

import java.io.Serializable;

/**
 * Sparse SVD dimensionality reducer. This reducer caches the U and S matrices
 * from the decomposition, but only enough to perform the dimensionality
 * reduction for the specified dimensionality. If it is later called to reduce
 * the dimensionality to a higher value, it may need to recompute the SVD,
 * therefore be sure to call it with the largest value first for maximum
 * performance.
 *
 * @deprecated svdlibj is broken and is unlikely to return correct results
 * @author bc73
 */
public class SparseSVD implements Reducer, Serializable {

    private static Logger log = Logger.getLogger(SparseSVD.class);
    private Matrix u;
    private SparseMatrix s;

    public Matrix reduce(Matrix m, int dim) {
        log.info("Reducing column dimensionality to " + dim);
        if (u == null || dim > u.getColumnCount()) {
            decompose(m, dim);
        }
        Matrix uReduced = u.select(Ret.LINK, String.format("*;0:%d", dim - 1));
        Matrix sReduced = s.select(Ret.LINK, String.format("0:%d;0:%d", dim - 1, dim - 1));
        return uReduced.mtimes(sReduced);
    }

    /**
     * Computes and caches the SVD decomposition of m.
     *
     * @param m matrix
     * @param dim the number of dimensions required
     */
    private void decompose(Matrix m, int dim) {
        log.info("Performing SVD decomposition");
        SMat sm = UJMPToSMat(m);
        SVDRec result = new Svdlib().svdLAS2A(sm, dim);
        u = DMatToUJMP(result.Ut).transpose(Ret.LINK);
        s = SparseMatrix.factory.zeros(result.S.length, result.S.length);
        for (int i = 0; i < result.S.length; i++) {
            s.setAsDouble(result.S[i], i, i);
        }
    }

    /**
     * Returns the number of nonzero entries in a matrix.
     *
     * @param m matrix
     * @return nonzero entries
     */
    private static int nonZeroEntries(Matrix m) {
        int count = 0;
        for (long[] coords : m.nonZeroCoordinates()) {
            if (m.getAsInt(coords) != 0) {
                count++;
            }
        }
        return count;
    }

    /**
     * Converts a UJMP matrix to a sparse svdlibj matrix.
     *
     * @param m matrix
     * @return s sparse svdlibj matrix
     */
    private static SMat UJMPToSMat(Matrix m) {
        int rows = (int) m.getRowCount();
        int cols = (int) m.getColumnCount();
        SMat s = new SMat(rows, cols, nonZeroEntries(m));
        for (int j = 0, n = 0; j < cols; j++) {
            s.pointr[j] = n;
            for (int i = 0; i < rows; i++) {
                double d = m.getAsDouble(i, j);
                if (d != 0) {
                    s.rowind[n] = i;
                    s.value[n] = d;
                    n++;
                }
            }
        }
        s.pointr[cols] = s.vals;
        return s;
    }

    /**
     * Wraps a dense svdlibj matrix with a UJMP matrix.
     *
     * @param dm dense svdlibj matrix
     * @return UJMP matrix
     */
    private static DenseMatrix DMatToUJMP(DMat dm) {
        return MatrixFactory.linkToArray(dm.value);
    }

    public static void main(String[] args) {
        double[][] values = {
            {1, 0, 0, 1, 0, 0, 0, 0, 0},
            {1, 0, 1, 0, 0, 0, 0, 0, 0},
            {1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 1, 0, 0, 0, 0},
            {0, 1, 1, 2, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 1, 1, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 1},
            {0, 0, 0, 0, 0, 0, 0, 1, 1}
        };
        Matrix m = MatrixFactory.importFromArray(values);
        SparseSVD s = new SparseSVD();
        s.reduce(m, 3);
        System.out.println(s.u);
        System.out.println(s.s);
    }
}
