/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.reducer;

import org.ujmp.core.Matrix;

/**
 * A class that can reduce the column dimensionality of a matrix.
 *
 * @author bc73
 */
public interface Reducer {

    /**
     * Reduces the the row dimensions of the matrix to a specified amount.
     *
     * @param m the input matrix to reduce
     * @param dim the row dimensionality of the new matrix
     * @return the dimensionality reduced matrix
     */
    public Matrix reduce(Matrix m, int dim);
}
