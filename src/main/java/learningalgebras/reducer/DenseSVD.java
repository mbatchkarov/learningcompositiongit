/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.reducer;

import org.apache.log4j.Logger;
import org.ojalgo.array.Array1D;
import org.ojalgo.matrix.decomposition.SingularValue;
import org.ojalgo.matrix.decomposition.SingularValueDecomposition;
import org.ujmp.core.Matrix;
import org.ujmp.core.calculation.Calculation.Ret;
import org.ujmp.core.matrix.DenseMatrix;
import org.ujmp.core.matrix.SparseMatrix;
import org.ujmp.ojalgo.OjalgoDenseDoubleMatrix2D;

import java.io.Serializable;

/**
 * Sparse SVD dimensionality reducer. This reducer caches the U and S matrices.
 *
 * @author bc73
 */
public class DenseSVD implements Reducer, Serializable {

    private static Logger log = Logger.getLogger(DenseSVD.class);
    private DenseMatrix u;
    private SparseMatrix s;

    public Matrix reduce(Matrix m, int dim) {
        if (m.getColumnCount() > dim) {
            String msg = "Reducing column dimensionality from %d to %d ";
        log.info(String.format(msg, m.getColumnCount(), dim));
            if (u == null) {
                decompose(m);
            }
            Matrix uReduced = u.select(Ret.LINK, String.format("*;0:%d", dim - 1));
            Matrix sReduced = s.select(Ret.LINK, String.format("0:%d;0:%d", dim - 1, dim - 1));
            return uReduced.mtimes(sReduced);
        } else {//no need for reduction
            return m;
        }
    }

    /**
     * Computes and caches the SVD decomposition of m.
     *
     * @param m matrix
     */
    private void decompose(Matrix m) {
//        log.info("Performing SVD decomposition");
        SingularValue<Double> svd = SingularValueDecomposition.makePrimitive();
        if (m instanceof OjalgoDenseDoubleMatrix2D) {
            svd.compute(((OjalgoDenseDoubleMatrix2D) m).getWrappedObject());
        } else {
            svd.compute(new OjalgoDenseDoubleMatrix2D(m).getWrappedObject());
        }
        u = new OjalgoDenseDoubleMatrix2D(svd.getQ1());
        Array1D<Double> singularValues = svd.getSingularValues();
        s = SparseMatrix.factory.zeros(singularValues.length, singularValues.length);
        for (int i = 0; i < singularValues.length; i++) {
            s.setAsDouble(singularValues.get(i), i, i);
        }
    }
}
