/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.reducer;

import org.apache.log4j.Logger;
import org.ujmp.core.Matrix;
import org.ujmp.core.matrix.SparseMatrix;

import java.util.Random;

/**
 * Reduces the dimensionality of a matrix using sparce random projections.
 *
 * @author bc73
 * @see Li, P., Hastie, T. J., and Church, K. W. 2006. Very sparse random
 * projections. In Proceedings of the 12th ACM SIGKDD international Conference
 * on Knowledge Discovery and Data Mining (Philadelphia, PA, USA, August 20 -
 * 23, 2006). KDD '06. ACM, New York, NY, 287-296.
 */
public class RandomProjection implements Reducer {

    private static Logger log = Logger.getLogger(RandomProjection.class);
    private final Random rnd;
    private final int s;

    /**
     * Creates a new random projection reducer.
     *
     * @param seed the seed to use for the random number generator
     * @param s sampling rate (expressed in terms of 1/s) -- suggested values
     * are 1, 3 or sqrt(number of columns in original matrix)
     */
    public RandomProjection(long seed, int s) {
        if (s < 1) {
            throw new IllegalArgumentException("s must be greater than 1");
        }
        this.rnd = new Random(seed);
        this.s = s;
    }

    public Matrix reduce(Matrix m, int dim) {
        if (dim < m.getColumnCount()) {
            String format = "Reducing column dimensionality to %d (s = %d)";
            log.info(String.format(format, dim, s));

            Matrix r = buildProjectionMatrix((int) m.getColumnCount(), dim);
            //final double invSqrtK = 1 / Math.sqrt(k);
            return m.mtimes(r); //.times(Ret.ORIG, true, invSqrtK);
        } else {
            return m; //no need to reduction of
        }
    }

    /**
     * Builds the projection matrix.
     *
     * @param rows the number of rows in the projection matrix
     * @param cols the number of columns in the projection matrix
     * @return projection matrix
     */
    private Matrix buildProjectionMatrix(int rows, int cols) {
        Matrix r = s > 1 ? SparseMatrix.factory.zeros(rows, cols)
                : Matrix.factory.zeros(rows, cols);
        final double sqrtS = Math.sqrt(s);
        for (int i = 0; i < rows; i++) {
            log.info("Processed " + i + " rows out of " + rows);
            int[] indices = getRandomIndices(rnd, s, cols);
            for (int j : indices) {
                int n = rnd.nextInt(2);
                if (n == 0) {
                    r.setAsDouble(sqrtS, i, j);
                } else if (n == 1) {
                    r.setAsDouble(-sqrtS, i, j);
                }
            }
        }
        return r;
    }

    private boolean contains(int[] values, int maxIndex, int check) {
        for (int i = 0; i < maxIndex; ++i) {
            if (values[i] == check) {
                return true;
            }
        }
        return false;
    }

    private int[] getRandomIndices(Random rnd, int n, int max) {
        int[] result = new int[n];

        for (int i = 0; i < n; ++i) {
            while (true) {
                int newIndex = rnd.nextInt(max);
                if (contains(result, i, newIndex)) {
                    continue;
                }
                result[i] = newIndex;
                break;
            }
        }

        return result;
    }
}
