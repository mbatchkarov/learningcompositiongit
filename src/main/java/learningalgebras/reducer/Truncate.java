/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.reducer;

import org.ujmp.core.Matrix;
import org.ujmp.core.calculation.Calculation.Ret;

/**
 * Very fast dimensionality reducer that simply truncates the matrix down
 * to size. Only meant for testing.
 *
 * @author bc73
 */
public class Truncate implements Reducer {

    public Matrix reduce(Matrix m, int dim) {
        return m.select(Ret.LINK, "*;0-" + (dim + 1));
    }
}
