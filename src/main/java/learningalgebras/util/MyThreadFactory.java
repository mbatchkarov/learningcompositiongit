/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.util;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * * A thread factory to be used in conjunction with thread pool executors. Provides
 * custom thread naming.
 * @see #MyThreadFactory(java.lang.String, java.lang.Object[], int) 
 * @author reseter
 */
public class MyThreadFactory implements ThreadFactory {

    private static final AtomicInteger poolNumber = new AtomicInteger(1);
    private final ThreadGroup group;
    private final AtomicInteger threadNumber = new AtomicInteger(0);
    private final Object[] ids;
    private final String purpose;
    private int priority;

    /**
      Threads will be pooled together and named
     * [purpose]-[identified]. It is assumed all threads in the pool serve the same purpose,
     * i.e. process different parts of the same task. Each of the subtasks can be named 
     * explicitly using the identifiers parameter- if null is passed, threads will be named
     * sequentially. If the number of identifiers passed is less than the actual number of 
     * threads created, all identifiers will be used and the remaining threads will be named
     * unnamed[number].
     * @param purpose The purpose of this thread pool. Recommended value: name of the task
     * @param identifiers Optional identifiers. If null, sequential numbers will be assigned
     * @param priority The priority of the newly constructed threads, e.g. Thread.MAX_PRIORITY
     */
    public MyThreadFactory(String purpose, Object[] identifiers, int priority) {
        this.ids = identifiers;
        this.purpose = purpose;
        this.priority = priority;

        SecurityManager s = System.getSecurityManager();
        group = (s != null) ? s.getThreadGroup()
                : Thread.currentThread().getThreadGroup();
    }

    public Thread newThread(Runnable r) {
        String id;
        if (ids == null) {
            id = "" + threadNumber.getAndIncrement();
        } else {
            try {
                id = ids[threadNumber.getAndIncrement()].toString().split("=")[0];
            }
            //if M inentifiers are provided, and N>M threads are created
            catch (ArrayIndexOutOfBoundsException ex) {
                id = "unnamed" + threadNumber.getAndIncrement();
            }
        }
        String name = purpose + "-" + id;
        Thread t = new Thread(group, r, name, 0);
        if (t.isDaemon()) {
            t.setDaemon(false);
        }
        t.setPriority(this.priority);
        return t;
    }
}
