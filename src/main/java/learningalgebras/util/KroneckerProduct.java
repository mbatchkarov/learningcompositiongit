/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/
package learningalgebras.util;

import org.ojalgo.matrix.store.MatrixStore;
import org.ojalgo.matrix.store.PhysicalStore;
import org.ojalgo.matrix.store.PrimitiveDenseStore;

public final class KroneckerProduct {
    MatrixStore<Double> first;
    MatrixStore<Double> second;

    public KroneckerProduct(MatrixStore<Double> first, MatrixStore<Double> second) {
        this.first = first;
        this.second = second;
    }

    public MatrixStore<Double> multiplyRight(MatrixStore<Double> m) {
        int rows = first.getRowDim()*second.getRowDim();
        int cols = m.getColDim();
        PhysicalStore<Double> result =
                PrimitiveDenseStore.FACTORY.makeEmpty(rows, cols);

        int thisCols = first.getColDim()*second.getColDim();
        for (int i=0; i<rows; ++i) {
            for (int j=0; j<cols; ++j) {
                double sum = 0.0;
                for (int k=0; k<thisCols; ++k) {
                    double r = get(i,k);
                    double s = m.get(k, j);
                    sum += r*s;
                }
                result.set(i, j, sum);
            }
        }

        return result;
    }

    public double get(int i, int j) {
        assert i < first.getRowDim()*second.getRowDim();
        int firstRow = i/second.getRowDim();
        int secondRow = i % second.getRowDim();
        assert j < first.getColDim()*second.getColDim();
        int firstCol = j/second.getColDim();
        int secondCol = j % second.getColDim();
        return first.get(firstRow, firstCol) * second.get(secondRow,secondCol);
    }
}
