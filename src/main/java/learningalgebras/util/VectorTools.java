/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.util;

import org.apache.log4j.Logger;

/**
 * Useful static methods for working with vectors (double[]).
 *
 * @author bc73
 */
public class VectorTools {

	private static final Logger log = Logger.getLogger(VectorTools.class);

	/**
	 * Static methods only.
	 */
	private VectorTools() {
	}


	/**
	 * Returns the row sum of the elements of the vector
	 *
	 * @param v input row vector
	 * @return sum(v, 1) in matlab notation
	 */
	public static double sum(double[] v) {
		double sum = 0;
		for (int i = 0; i < v.length; i++) {
			sum += v[i];
		}
		return sum;
	}

	public static int sum(int[] v) {
		int sum = 0;
		for (int i = 0; i < v.length; i++) {
			sum += v[i];
		}
		return sum;
	}

	/**
	 * Returns the Manhattan norm of a vector.
	 *
	 * @param v input vector
	 * @return ||v||_1
	 */
	public static double l1Norm(double[] v) {
		double sum = 0;
		for (int i = 0; i < v.length; i++) {
			sum += Math.abs(v[i]);
		}
		return sum;
	}

	/**
	 * Returns the Euclidean norm of a vector.
	 *
	 * @param v input vector
	 * @return ||v||_2
	 */
	public static double l2Norm(double[] v) {
		double sum = 0;
		for (int i = 0; i < v.length; i++) {
			sum += v[i] * v[i];
		}
		return Math.sqrt(sum);
	}

	/**
	 * Adds one vector to another.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return u + v
	 */
	public static double[] add(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double[] w = new double[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = u[i] + v[i];
		}
		return w;
	}

	public static int[] add(int[] u, int[] v) {
		assertEqualDimensionality(u, v);
		int[] w = new int[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = u[i] + v[i];
		}
		return w;
	}

	/**
	 * Component-wise multiplication.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return u .* v
	 */
	public static double[] mul(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double[] w = new double[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = u[i] * v[i];
		}
		return w;
	}

	/**
	 * Subtracts one vector from another.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return u - v
	 */
	public static double[] subtract(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double[] w = new double[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = u[i] - v[i];
		}
		return w;
	}

	public static int[] subtract(int[] u, int[] v) {
		assertEqualDimensionality(u, v);
		int[] w = new int[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = u[i] - v[i];
		}
		return w;
	}

	/**
	 * Returns the component-wise maximum between two vectors.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return component-wise maximum of u and v
	 */
	public static double[] max(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double[] w = new double[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = Math.max(u[i], v[i]);
		}
		return w;
	}

	/**
	 * Returns the component-wise minimum between two vectors.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return component-wise minimum of u and v
	 */
	public static double[] min(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double[] w = new double[u.length];
		for (int i = 0; i < w.length; i++) {
			w[i] = Math.min(u[i], v[i]);
		}
		return w;
	}

	/**
	 * Returns the dot product of two vectors.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return u • v
	 */
	public static double dotProduct(double[] u, double[] v) {
		assertEqualDimensionality(u, v);
		double sum = 0;
		for (int i = 0; i < u.length; i++) {
			sum += u[i] * v[i];
		}
		return sum;
	}

	/**
	 * Returns the cosine of the angle between two vectors.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @return cos(θ)
	 */
	public static double cosine(double[] u, double[] v) {
		return dotProduct(u, v) / (l2Norm(u) * l2Norm(v));
	}

	/**
	 * Returns a unit vector for the specified vector. The original vector is
	 * unchanged.
	 *
	 * @param v vector
	 * @return unit vector of v
	 */
	public static double[] unitVector(double[] v) {
		double[] u = v.clone();
		normalize(u);
		return u;
	}

	/**
	 * Normalises a vector to unit length.
	 *
	 * @param v normalised vector
	 */
	public static void normalize(double[] v) {
		double n = l2Norm(v);
		for (int i = 0; i < v.length; i++) {
			v[i] /= n;
		}
	}

	/**
	 * Creates a random vector for a specified dimensionality.
	 *
	 * @param dimensions the dimensionality of the vector
	 * @param lower      lower limit of random number
	 * @param upper      upper limit of random number
	 * @return a random vector
	 */
	public static double[] random(int dimensions, double lower, double upper) {
		double[] v = new double[dimensions];
		for (int i = 0; i < dimensions; i++) {
			v[i] = Math.random() * (upper - lower) + lower;
		}
		return v;
	}

	/**
	 * Maps function f onto each component of v.
	 *
	 * @param f the function to apply
	 * @param v the vector to apply the function to
	 * @return the modified vector
	 */
	public static double[] map(Function f, double[] v) {
		for (int i = 0; i < v.length; i++) {
			v[i] = f.apply(v[i]);
		}
		return v;
	}

	/**
	 * Asserts that two vectors are of the same dimensionality.
	 *
	 * @param u first vector
	 * @param v second vector
	 * @throws IllegalArgumentException if the dimensions of u and v are not equal
	 */
	private static void assertEqualDimensionality(double[] u, double[] v) {
		if (u.length != v.length) {
			log.error(u.length);
			log.error(v.length);
			throw new IllegalArgumentException("Unequal vector dimensions");
		}
	}

	private static void assertEqualDimensionality(int[] u, int[] v) {
		if (u.length != v.length) {
			log.error(u.length);
			log.error(v.length);
			throw new IllegalArgumentException("Unequal vector dimensions");
		}
	}

	/**
	 * Interface for defining component-wise functions.
	 */
	public interface Function {

		public double apply(double x);
	}
}
