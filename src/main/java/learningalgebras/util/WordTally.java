/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.util;

import java.util.*;

/**
 * Class for keeping a tally of the number of times a word has been seen.
 *
 * @author bc73
 */
public class WordTally {

    private Map<String, Integer> tally;

    /**
     * Creates an empty word tally.
     */
    public WordTally() {
        tally = new HashMap<String, Integer>();
    }

    /**
     * Increments the count for a word.
     *
     * @param word the word
     */
    public void increment(String word) {
        if (tally.containsKey(word)) {
            tally.put(word, tally.get(word) + 1);
        } else {
            tally.put(word, 1);
        }
    }

    /**
     * Returns the a set of most popular words, ie a set containing those words
     * with the highest tallies.
     *
     * @param size the size of the set to return
     * @param start the number of most popular words to omit
     * @return set of the most popular words
     */
    public SortedSet<String> getMostPopular(int start, int size) {
        Map<String, Integer> sorted = CollectionUtils.sortByValue(tally, start, start + size, true);
        return new TreeSet<String>(sorted.keySet());
    }

    /**
     * Returns the set of most popular words above a certain popularity
     * threshold.
     *
     * @param size the size of the set to return
     * @param minimum exclude words whose tally is below this threshold
     * @return set of the most popular words
     */
    public Set<String> getAboveThreshold(int size, int minimum) {
        Map<String, Integer> threshold = new HashMap<String, Integer>();
        for (Map.Entry<String, Integer> entry : tally.entrySet()) {
            if (entry.getValue() >= minimum) {
                threshold.put(entry.getKey(), entry.getValue());
            }
        }
        Map<String, Integer> sorted = CollectionUtils.sortByValue(threshold, size, true);
        return new HashSet<String>(sorted.keySet());
    }
}
