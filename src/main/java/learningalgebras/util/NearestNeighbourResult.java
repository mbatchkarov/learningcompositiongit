package learningalgebras.util;

/**
 * Copyright
 * User: mmb28
 * Date: 14/01/2013
 * Time: 16:42
 * To change this template use File | Settings | File Templates.
 */
public class NearestNeighbourResult {
	private String composer, adj, noun, nearestNeigh;
	private double sim;
	private int rank;

	public NearestNeighbourResult(String composer, String adj, String noun, String firstNeigh, double sim, int rank) {
		this.composer = composer;
		this.adj = adj;
		this.noun = noun;
		this.nearestNeigh = firstNeigh;
		this.sim = sim;
		this.rank = rank;
	}

	public String toString(){
		StringBuilder b = new StringBuilder();
		b.append(composer.toString()).append(", ").append(adj).append(", ");
		b.append(noun).append(", ").append(nearestNeigh).append(", ");
		b.append(sim).append(", ").append(rank);
		return b.toString();
	}

	public int getRank() {
		return rank;
	}

}
