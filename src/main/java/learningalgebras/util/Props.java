/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.util;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;

/**
 * Properties class- hold a single public static apache Configuration object containing all program settings
 *
 * @author mmb28
 */
public class Props {

	private static final Logger log = Logger.getLogger(Props.class);

	private static Configuration p;


	public static void readConfig(String file) {
		try {
			p = new PropertiesConfiguration(file);
			if (!p.containsKey("name"))
				throw new IllegalArgumentException("Configuration file does not contain an experiment name");

			final String filename = new File(file).getName();
			final String desdir = p.getString("data.outdir", ".") + "/";
			FileUtils.copyFile(new File(file), new File(desdir + filename));

		} catch (ConfigurationException e) {
			log.error("Error while trying to create configuration object");
			e.printStackTrace();
		} catch (IOException e) {
			log.error("Error while trying to copy conf file to output directory");
			e.printStackTrace();
		}
	}

	public static Configuration getProps() {
		if (p == null) {
			log.warn("CONFIGURATION OBJECT IS NULL, USING DEFAULT");
			readConfig("default.props.txt");
		}
		return p;
	}
}
