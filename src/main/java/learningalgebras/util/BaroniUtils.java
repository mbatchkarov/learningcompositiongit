/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.util;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author mmb28
 */
public class BaroniUtils {

	private static final Logger log = Logger.getLogger(BaroniUtils.class);
	private static SortedSet<String> interestingAdjectives, interestingBigrams;
	private static String baroniOutputPrefix = Props.getProps().getString("baroni.training.outDir");

	static {
		interestingAdjectives = readInterestingAdjectives();
		interestingBigrams = readInterestingBigrams();
	}
	
	
	public static void setInterestingWords(Set<String> adjectives, Set<String> bigrams){
		interestingAdjectives = new TreeSet<String>(adjectives);
		interestingBigrams = new TreeSet<String>(bigrams);
	}

	/**
	 * Returns a set of all adjectives that we want to learn a matrix for. These
	 * are contained in composeMe.txt and have been seen often and together
	 * with at least 200 nouns
	 *
	 * @return
	 */
	public static SortedSet<String> readInterestingAdjectives() {
		log.info("Reading adjective list");
		BufferedReader reader = null;
		SortedSet<String> adjectives = new TreeSet<String>();
		try {
			reader = new BufferedReader(new FileReader(
			Props.getProps().getString("baroni.training.adjNounFile")));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] words = line.split("_");
				adjectives.add(words[0]);
			}
		} catch (IOException e) {
			log.error(e);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				log.error("Could not close stream");
			}
		}


		if (Props.getProps().getBoolean("pipeline.testRun")) {
			adjectives.clear();
			adjectives.add("American-j");
			adjectives.add("new-j");
//			adjectives.add("great-j");
//			adjectives.add("large-j");
//			adjectives.add("different-j");
		}


		log.info("Found " + adjectives.size() + " interesting adjectives");
		return Collections.unmodifiableSortedSet(adjectives);
	}

	public static SortedSet<String> readInterestingBigrams() {
		log.info("Reading bigram list");
		BufferedReader reader = null;
		SortedSet<String> bigrams = new TreeSet<String>();
		try {
			reader = new BufferedReader(new FileReader(
			Props.getProps().getString("baroni.training.adjNounFile")));
			String line;
			while ((line = reader.readLine()) != null) {
				String[] words = line.split("_");
				if (interestingAdjectives.contains(words[0])) {
					//only include bigrams of known adjectives
					bigrams.add(line.replaceAll("_", " "));
				}
			}
		} catch (IOException e) {
			log.error(e);
		} finally {
			try {
				reader.close();
			} catch (IOException ex) {
				log.error("Could not close stream");
			}
		}

		log.info("Found " + bigrams.size() + " interesting bigrams");
		return Collections.unmodifiableSortedSet(bigrams);
	}

	/**
	 * Checks if all files that should have been output by the training procedure
	 * for a specific adjectives are in place (<adjective>.betas)
	 *
	 * @param adjsToCompose the adjective
	 * @return true if the files are there
	 */
	public static boolean modelFilesInPlace(Set<String> adjsToCompose) {
		String prefix = baroniOutputPrefix;
		String[] files = new File(prefix).list();

		int matchedAdjectives = 0;

		for (int j = 0; j < files.length; j++) {

			File f = new File(prefix + files[j]);
			if (f.isDirectory()) {
				continue;//NPE because of .DS_Store file on OS X
			}

			if (f.getName().endsWith(".betas")) {
				String[] namePieces = f.getName().replaceAll("\\.betas", "").split("/");
				String name = namePieces[namePieces.length - 1];
				if (adjsToCompose.contains(name + "-j")) {
					matchedAdjectives++;
				}
			}
		}

		return matchedAdjectives == adjsToCompose.size();
	}

	public static SortedSet<String> getInterestingAdjectives() {
		if (interestingAdjectives == null)
			interestingAdjectives = readInterestingAdjectives();
		return interestingAdjectives;

	}

	public static Set<String> getInterestingBigrams() {
		if (interestingBigrams == null)
			interestingBigrams = readInterestingBigrams();
		return interestingBigrams;
	}
}
