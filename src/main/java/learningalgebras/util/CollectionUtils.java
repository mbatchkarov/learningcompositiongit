/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.util;

import java.util.*;

/**
 * Useful static methods for working with collections.
 *
 * @author bc73
 */
public class CollectionUtils {

    /**
     * Static methods only.
     */
    private CollectionUtils() {
    }

    /**
     * Prints all key/values pairs in a map one per line.
     *
     * @param m the map to print
     */
    public static void printMap(Map<?, ?> m) {
        for (Map.Entry<?, ?> e : m.entrySet()) {
            System.out.println(e.getKey() + " " + e.getValue());
        }
    }

    /**
     * Sorts a map by its values. The original map is unmodified.
     *
     * @param map the map to sort
     * @param start the number of highest value items to omit
     * @param limit the maximum number of entries in the sorted map
     * @param reverse set to true if the map should be sorted in descending order
     * @return map sorted by values with a size no greater than limit
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, int start, int limit, final boolean reverse) {
        List<Map.Entry<K, V>> list = new ArrayList<Map.Entry<K, V>>(map.entrySet());
        Collections.sort(list, new Comparator<Map.Entry<K, V>>() {

            public int compare(Map.Entry<K, V> o1, Map.Entry<K, V> o2) {
                return reverse ? o2.getValue().compareTo(o1.getValue())
                        : o1.getValue().compareTo(o2.getValue());
            }
        });
        Map<K, V> result = new LinkedHashMap<K, V>();
        for (int i = start; i != limit && i < list.size(); i++) {
            Map.Entry<K, V> entry = list.get(i);
            result.put(entry.getKey(), entry.getValue());
        }
        return result;
    }

    /**
     * Sorts a map by its values. The original map is unmodified.
     *
     * @param map the map to sort
     * @param limit the maximum number of entries in the sorted map
     * @param reverse set to true if the map should be sorted in descending order
     * @return map sorted by values with a size no greater than limit
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, int limit, final boolean reverse) {
        return sortByValue(map, 0, limit, reverse);
    }

    /**
     * Sorts a map by its values. The original map is unmodified.
     *
     * @param map the map to sort
     * @param reverse set to true if the map should be sorted in descending order
     * @return map sorted by values
     */
    public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map, final boolean reverse) {
        return sortByValue(map, -1, reverse);
    }

    /**
     * Partitions a list into equal sized sublists. The sublists are views of
     * the original list.
     *
     * @param l the list to partition
     * @param n the number of partitions
     * @return the partitioned list
     * @throws IllegalArgumentException if n < 1
     */
    public static <T> List<List<T>> partitionList(List<T> l, int n) {
        if (n < 1) {
            String msg = "Number of partitons must be greater than 0";
            throw new IllegalArgumentException(msg);
        }
        List<List<T>> partitions = new ArrayList<List<T>>(n);
        for (int i = 0; i < n; i++) {
            int fromIndex = Math.round((float) l.size() / n * i);
            int toIndex = Math.round((float) l.size() / n * (i + 1));
            partitions.add(i, l.subList(fromIndex, toIndex));
        }
        return partitions;
    }

    /**
     * Randomly partitions a collection into equal sized lists.
     *
     * @param c the collection to partition
     * @param n the number of partitions
     * @param rnd source of randomness
     * @return the partitioned set
     * @throws IllegalArgumentException if n < 1
     */
    public static <T> List<List<T>> randomPartition(Collection<T> c, int n, Random rnd) {
        List<T> adjList = new ArrayList<T>(c);
        Collections.shuffle(adjList, rnd);
        return partitionList(adjList, n);
    }

    /**
     * Returns the sum of a collection of number.
     *
     * @param c collection of numbers
     * @return sum of this collection
     */
    public static double sum(Collection<? extends Number> c) {
        double sum = 0;
        for (Number n : c) {
            sum += n.doubleValue();
        }
        return sum;
    }

    /**
     * Returns the mean value of a collection of numbers.
     *
     * @param c collection of numbers
     * @return mean of this collection
     */
    public static double average(Collection<? extends Number> c) {
        return sum(c) / c.size();
    }
}
