/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras;

import learningalgebras.composer.*;
import learningalgebras.eval.ComposerEvaluator;
import learningalgebras.eval.similarity.*;
import learningalgebras.reducer.DenseSVD;
import learningalgebras.reducer.RandomProjection;
import learningalgebras.reducer.Reducer;
import learningalgebras.util.Props;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.log4j.Logger;

import java.io.*;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 * @author bc73
 */
public class Main {

	private static final Logger log = Logger.getLogger(Main.class);
	public static final String COMPOSER_TMP_TXT = "composer.tmp.txt";

	/**
	 * The main class
	 *
	 * @param args file containing phrases
	 * @throws ConfigurationException if the configuration file cannot be found or is not
	 *                                properly formatted
	 */
	public static void main(String[] args) throws ConfigurationException, IOException {
		System.out.println("started");
//        clearOldLogs();
		final String confFile = System.getProperty("user.dir") + "/" + args[0];
		log.info("Running with parameter file " + confFile);
		Props.readConfig(confFile);
//        ProcessBuilder builder = new ProcessBuilder("./getJobId.sh");//if running on cluster
//        try {
//            Process process = builder.start();
//            process.waitFor();
//        } catch (IOException e) {
//            log.error("Could not get job id. Is this sge?" + builder.command(), e);
//        } catch (InterruptedException ex) {
//            log.error(ex);
//        }

		Reducer reducer = selectReducer();
		SortedSet<Similarity> comparators = selectComparators();
		BigramComposer composer = selectComposer();


		//------------- set up sumilation ------------------------
		String path = Props.getProps().getString("data.file");
		boolean baroni = Props.getProps().getBoolean("data.baroni", true);
		boolean load = Props.getProps().getBoolean("data.load", true);
		int numAdjNoun = Props.getProps().getInt("data.numAdjNoun", 50);

		int numPartitions = Props.getProps().getInt("cv.number", 100);
		int seed = Props.getProps().getInt("pipeline.seed", 0);

		int min = Props.getProps().getInt("reducer.min", 300);
		int max = Props.getProps().getInt("reducer.max", 300);
		int step = Props.getProps().getInt("reducer.step", 10);

		int currentDim = Props.getProps().getInt("data.dim", 300);

		FeatureVectorMatrix original = getData(path, baroni, load, numAdjNoun, max, reducer, currentDim);
		ComposerEvaluator ev = new ComposerEvaluator(comparators);

		for (int dims = max; dims >= min; dims -= step) {//different dim. reduction settings
			log.info(String.format("Using %d dimensions", dims));

			FeatureVectorMatrix reduced = new FeatureVectorMatrix(original, reducer, dims);
			reduced.newPartitionData(numPartitions, seed);

			String msg = String.format("Training begins with a total of %d adjectives, %d nouns and %d valid bigrams ",
			reduced.getAdjs().size(), reduced.getNouns().size(), reduced.getPairs().size());
			log.info(msg);

			if (composer instanceof AlgebraComposer) {
				int components = reduced.getVectorDimensionality();
				if (reduced.getPairs().size() < components * components) {
					log.warn("Algebra composer: Skipping term eval due to underdetermined system");
					log.warn("Num training pairs = " + reduced.getPairs().size() + " < dim^2 = " + (components * components));
					continue;
				}
			}

			//make sure partitions are done in order-- makes progress tracking easier
			SortedSet<Integer> sortedPartitionIDs = new TreeSet<Integer>(reduced.getFoldPartitions().keySet());
			for (Integer foldNo : sortedPartitionIDs) {
				Partition p = reduced.getFoldPartitions().get(foldNo);
				composer.train(reduced, p);
				if (Props.getProps().getBoolean("composers.serialize", false)) {
					Main.save(composer, COMPOSER_TMP_TXT);
//					composer = (BigramComposer) Main.load(COMPOSER_TMP_TXT);
				}

				ev.evaluate(composer, reduced, p);
			}
			log.info("Successfully completed this train-test round");

		}
		ev.shutdown();
		System.exit(0);//force quit, ignoring any lingering threads-- these are in the pool of nearest neighbour study 2

	}

	/**
	 * parse the FVM from file or create it from a TCM
	 * Side effect: the value of the {@code max} parameter can be reduced if the provided file contains only
	 * lower-dimensionality data, e.g. you ask for 300 but file has 50, then max <-50
	 *
	 * @return
	 */
	public static FeatureVectorMatrix getData(String path, boolean baroni, boolean load, int numAdjNoun, int max,
	                                          Reducer reducer, int currentDimensionality) throws IOException {
		FeatureVectorMatrix original;
		boolean correctDimensionality = false;
		do {
			int i = path.lastIndexOf(".");
			String name = path.substring(0, i);
			File cachedFile = new File(name + ".bin");//try to load cached version from disk
			boolean newCachedFileJustWritten = false;

			if (baroni) {
				if (cachedFile.exists() && load) {
					original = (FeatureVectorMatrix) Main.load(cachedFile.getAbsolutePath());
				} else {
					original = new FeatureVectorMatrix(path, reducer, max, currentDimensionality);
					Main.save(original, cachedFile.getAbsolutePath());
					newCachedFileJustWritten = true;
				}
			} else {
				if (cachedFile.exists() && Props.getProps().getBoolean("data.load")) {
					original = (FeatureVectorMatrix) Main.load(cachedFile.getAbsolutePath());
				} else {
					original = new FeatureVectorMatrix(
					new TermContextMatrix(path, numAdjNoun, numAdjNoun, 300, 300, 20),
					reducer, max);
					Main.save(original, cachedFile.getAbsolutePath());
					newCachedFileJustWritten = true;
				}
			}
			if (original.getMatrix().getColumnCount() < max) {
				String msg = "You requested %d dimensions but the loaded data set only has %d.";
				log.warn(String.format(msg, max, original.getMatrix().getColumnCount()));
				if (cachedFile.exists() && (!newCachedFileJustWritten)) {
					log.warn("Removing cached file and re-parsing plain text file!");
					cachedFile.delete();
				} else {
					log.warn("The issue is not due to my caching of data. Overriding your choice of dimensionality");
					max = (int) original.getMatrix().getColumnCount();
				}
			} else {
				correctDimensionality = true;
			}

		} while (!correctDimensionality);
		return original;
	}

	private static BigramComposer selectComposer() {
		BigramComposer composer = null;
		String type = Props.getProps().getString("composer.type", "add");
		if (Props.getProps().getList("composer.type").size() > 1) {
			log.warn("Multiple composers specified, selecting one");
		}
		if (type.contains("algebra")) {
			composer = (new AlgebraComposer());
		} else if (type.contains("add")) {
			composer = (new AddComposer());
		} else if (type.contains("mult")) {
			composer = (new MultiplyComposer());
		} else if (type.contains("baroni")) {
			composer = (new NewBaroniComposer());
		} else if (type.contains("noun")) {
			composer = (new IgnoreFirstComposer());
		} else if (type.contains("adj")) {
			composer = (new IgnoreSecondComposer());
		} else {
			throw new IllegalArgumentException("Unrecognised composer type, please check conf file");
		}
		log.info("Composer is " + composer.toString());
		return composer;
	}

	private static SortedSet<Similarity> selectComparators() {
		//------------- set up comparators ------------------------
		// map of the simularity comparison methods to use for evaluation
		SortedSet<Similarity> comparators = new TreeSet<Similarity>();
		if (Props.getProps().getBoolean("comp.cosine", false)) {
			comparators.add(new Cosine());
		}
		if (Props.getProps().getBoolean("comp.l1", false)) {
			comparators.add(new L1Dist());
		}
		if (Props.getProps().getBoolean("comp.l2", false)) {
			comparators.add(new L2Dist());
		}
		if (Props.getProps().getBoolean("comp.norm", false)) {
			comparators.add(new NormDifference());
		}
		if (Props.getProps().getBoolean("comp.ent1", false)) {
			comparators.add(new Ent1());
		}
		if (Props.getProps().getBoolean("comp.ent2", false)) {
			comparators.add(new Ent2());
		}
		return comparators;
	}

	private static Reducer selectReducer() {
		Reducer reducer = null;
		if (Props.getProps().getString("reducer.method", "svd").equals("svd")) {
			reducer = new DenseSVD();
		}
		if (Props.getProps().getString("reducer.method", "svd").equals("RandomProjection")) {
			reducer = new RandomProjection(Props.getProps().getInt("pipeline.seed", 0),
			Props.getProps().getInt("reducer.rp.s", 0));
		}
		if (reducer == null) {
			log.error("No reducer specified!");
			System.exit(0);
		}
		return reducer;
	}

	/**
	 * Load a serialised object from disc
	 *
	 * @param filename path to file
	 * @return the object
	 */
	public static Object load(String filename) {
		log.info("Loading " + filename);
		Object obj = null;
		try {
			FileInputStream fis = new FileInputStream(filename);
			ObjectInputStream in = new ObjectInputStream(fis);
			obj = in.readObject();
			in.close();
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (ClassNotFoundException e) {
			log.error(e.getMessage());
		}
		return obj;
	}

	/**
	 * Saves a serialisable object to disc.
	 *
	 * @param obj      the object to save
	 * @param filename path to file
	 */
	public static void save(Serializable obj, String filename) {
		log.info("Saving " + obj.toString() + " to " + filename);
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			ObjectOutputStream out = new ObjectOutputStream(fos);
			out.writeObject(obj);
			out.close();
		} catch (IOException e) {
			log.error(e.getMessage());
		}

	}

	public static void clearOldLogs() {
		String prefix = "./logs/";
		String[] logs = new File(prefix).list();
		for (int j = 0; j < logs.length; j++) {

			File f = new File(prefix + logs[j]);
			if (f.exists()) {
				f.delete();
			}
		}
	}

}
