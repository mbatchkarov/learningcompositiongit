/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.eval;

import learningalgebras.VectorStore;
import learningalgebras.composer.BigramComposer;
import learningalgebras.util.NearestNeighbourResult;
import learningalgebras.util.VectorTools;
import org.apache.log4j.Logger;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;

/**
 * A worker class which
 * 1. Composes an adjective and a noun
 * 2. Spawn other workers (@see learningalgebras.eval.NNMapWorker) to calculate
 * in parallel the distance between the result and a predefined list of other item
 * 3. Calculates the rank of a desired output in the list of nearest neighbors and
 * writes to disk
 *
 * @author mmb28
 */
public class NNStudyControlANDReduceWorker implements Runnable {

	private static final Logger log = Logger.getLogger(NNStudyControlANDReduceWorker.class);
	private String testBigram;
	private BigramComposer composer;
	private VectorStore fvm;
	private ExecutorService distanceCalculators;
	private CountDownLatch latchToNotifyWhenDone;
	private int numWorkers, id;
	private List<NearestNeighbourResult> out;

	public NNStudyControlANDReduceWorker(String testBigram, BigramComposer composer, VectorStore fvm,
	                                     ExecutorService distanceCalculators, int numWorkers, CountDownLatch latch,
	                                     int id, List<NearestNeighbourResult> out) {
		this.testBigram = testBigram;
		this.composer = composer;
		this.fvm = fvm;
		this.distanceCalculators = distanceCalculators;
		this.latchToNotifyWhenDone = latch;
		this.numWorkers = numWorkers;
		this.id = id;
		this.out = out;
	}

	public void run() {
		try {
			testBigram = testBigram.replace("_", " ");
			double[] composedVector = null;
			try {
				composedVector = composer.compose(fvm, testBigram.split(" ")[0], testBigram.split(" ")[1]);
				if (composedVector == null) {
					throw new IllegalArgumentException("Result of composition is null");
				}
			} catch (java.lang.IllegalArgumentException exception) {
				//either because composer failed (e.g. no vector for this word) or because
				//it returned null
				log.error("Could not compose bigram " + testBigram + ". Moving on");
				exception.printStackTrace();
			}


            /* stores distances between all observed vectors and a "target vector"
             * keep in mind the target is the result of a composition and is distinct
             * from the observed vector for the same bigram, so distances shouldn't be cached 
             * for efficiency */
			final ConcurrentMap<String, Double> distToPhraseVector = new ConcurrentHashMap<String, Double>();
			int size = fvm.getAllTerms().size();
			String[] allWords = new String[size];
			allWords = fvm.getAllTerms().toArray(allWords);
//            log.trace("Looking for neighbours in a list of size " + allWords.length);
			CountDownLatch latchForDistanceWorkers = new CountDownLatch(numWorkers);

			// Train each composer on the subset
			for (int i = 0; i < numWorkers; i++) {
				int beg = Math.max(i * (size / numWorkers) - 15, 0);//don't go below 0
				int end = Math.min((i + 1) * (size / numWorkers) + 15, size);//don't go out of bounds
				//+/- 15 introduces a bit of overlap to make sure no distances are omitted due to rounding error
				distanceCalculators.submit(new NNMapWorker(composedVector, distToPhraseVector,
				allWords, beg, end, fvm, latchForDistanceWorkers));
			}

			try {
				latchForDistanceWorkers.await(); //wait for all workers to finish before proceeding with the next iteration
			} catch (InterruptedException ex1) {
				log.error(ex1);
			}

			assert allWords.length == distToPhraseVector.size();
			for (int i = 0; i < allWords.length; i++) {
				String token = allWords[i];
				assert distToPhraseVector.containsKey(token);
			}//confirm distance to all observed vectors has been calculated

			//compute distances to all
			PriorityQueue<String> q = new PriorityQueue<String>(fvm.getAllTerms().size(), new Comparator<String>() {

				public int compare(String o1, String o2) {
					try {
						assert o1 != null;
						assert o2 != null;
						assert distToPhraseVector.containsKey(o1);
						assert distToPhraseVector.containsKey(o2);
					} catch (AssertionError e) {
						log.error(o1);
						log.error(o1);
						throw e;
					}
					double cos1 = distToPhraseVector.get(o1);
					double cos2 = distToPhraseVector.get(o2);

					return cos1 > cos2 ? -1 : 1;

				}
			});
			q.addAll(fvm.getAllTerms());

			String adj = testBigram.split(" ")[0];
			String noun = testBigram.split(" ")[1];
			String firstNeighbour = q.peek();
			double sim = VectorTools.cosine(fvm.getTermVector(testBigram), fvm.getTermVector(firstNeighbour));
			//compute rank of observed vector in list of NN
			boolean observedFound = false;
			int rank = 0;
			String nextNeighbour;
			while (!observedFound && rank < 1000) {
				nextNeighbour = q.poll();
				if (nextNeighbour.equals(testBigram)) {
					observedFound = true;
				}
				rank++;
			}

			out.add(new NearestNeighbourResult(composer.toString(), adj, noun, firstNeighbour, sim, rank));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e);
		} finally {
			latchToNotifyWhenDone.countDown();
		}
	}
}
