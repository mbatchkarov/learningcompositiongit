/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.eval;

import learningalgebras.VectorStore;
import learningalgebras.util.VectorTools;
import org.apache.log4j.Logger;

import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;

/**
 * Computes cosine similarity between a target vector and a list of other vectors.
 * Results are added to a predefined map
 * @author mmb28
 */
public class NNMapWorker implements Runnable {

    private static final Logger log = Logger.getLogger(NNMapWorker.class);
    private ConcurrentMap<String, Double> dist;
    private String[] words;
	private int startIndex, endIndex;
    private VectorStore fvm;
    private double[] phraseVector;
    private CountDownLatch latch;
	

    public NNMapWorker(double[] wordVector, ConcurrentMap<String, Double> map, 
            String[] words, int startIndex, int endIndex, VectorStore fvm, CountDownLatch latch) {
        dist = map;
        this.words = words;
	    this.startIndex = startIndex;
	    this.endIndex = endIndex;
        this.fvm = fvm;
        this.phraseVector = wordVector;
        this.latch = latch;
    }

    public void run() {
        try {
            for (int i = startIndex; i < endIndex; i++) {
                String word = words[i];
                //TODO normalization does not affect the angle between the vectors!!!     however, it might affect other similarity measures
                double[] v1 = fvm.getTermVector(word);
                double cos = VectorTools.cosine(v1, phraseVector);
                dist.put(word, cos);
            }
        } catch (Exception ex) {
            log.error(ex);
        }
        finally{
            latch.countDown();
        }
    }
}
