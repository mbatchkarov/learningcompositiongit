/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.l2Norm;
import static learningalgebras.util.VectorTools.subtract;

/**
 * Computes the Euclidean distance between two vectors.
 *
 * @author bc73
 */
public class L2Dist extends Similarity {

    public double getSimilarity(double[] u, double[] v) {
        return l2Norm(subtract(u, v));
    }
}
