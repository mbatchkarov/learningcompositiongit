/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

/**
 * Interface for class that compute the similarity between two vectors.
 *
 * @author bc73
 */
public abstract class Similarity implements Comparable<Similarity>{
    
    private String name = this.getClass().getSimpleName();
     /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return name;
    }
    
    @Override
    public int compareTo(Similarity o){
        return o.getName().compareTo(name);
    }

    /**
     * Computes the similarity between two vectors.
     *
     * @param u first vector
     * @param v second vector
     * @return some value to indicate similarity whose meaning depends on the
     * implementation
     */
    public abstract double getSimilarity(double[] u, double[] v);
    
}
