/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.*;

/**
 * Computes the entailment measure between two vectors.
 *
 * @author bc73
 */
public class Ent2 extends Similarity {

    private static final Function plus = new Function() {

        public double apply(double x) {
            return Math.max(0, x);
        }
    };

    public double getSimilarity(double[] u, double[] v) {
        return l1Norm(min(map(plus, u), map(plus, v))) / l1Norm(u);
    }
}
