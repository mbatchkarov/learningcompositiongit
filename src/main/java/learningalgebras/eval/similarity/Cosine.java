/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.cosine;

/**
 * Computes the cosine similarity between two vectors.
 *
 * @author bc73
 */
public class Cosine extends Similarity {

    public double getSimilarity(double[] u, double[] v) {
        return cosine(u, v);
    }
}
