/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.l2Norm;

/**
 * Computes the cosine similarity between two vectors.
 *
 * @author bc73
 */
public class NormDifference extends Similarity {

    public double getSimilarity(double[] u, double[] v) {
        return l2Norm(v) - l2Norm(u);
    }
}
