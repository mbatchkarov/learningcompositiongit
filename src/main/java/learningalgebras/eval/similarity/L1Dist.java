/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.l1Norm;
import static learningalgebras.util.VectorTools.subtract;

/**
 * Computes the Manhattan distance between two vectors.
 *
 * @author bc73
 */
public class L1Dist extends Similarity {

    public double getSimilarity(double[] u, double[] v) {
        return l1Norm(subtract(u, v));
    }
}
