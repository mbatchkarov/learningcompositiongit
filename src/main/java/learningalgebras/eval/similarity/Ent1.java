/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval.similarity;

import static learningalgebras.util.VectorTools.*;

/**
 * Computes the entailment measure between two vectors.
 *
 * @author bc73
 */
public class Ent1 extends Similarity {

    private static final Function abs = new Function() {

        public double apply(double x) {
            return Math.abs(x);
        }
    };

    public double getSimilarity(double[] u, double[] v) {
        return l1Norm(min(map(abs, u), map(abs, v))) / l1Norm(u);
    }
}
