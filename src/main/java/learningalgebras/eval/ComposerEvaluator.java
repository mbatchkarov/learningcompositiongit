/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.eval;

import learningalgebras.Partition;
import learningalgebras.VectorStore;
import learningalgebras.composer.BigramComposer;
import learningalgebras.eval.similarity.Similarity;
import learningalgebras.util.BaroniUtils;
import learningalgebras.util.NearestNeighbourResult;
import learningalgebras.util.Props;
import learningalgebras.util.VectorTools;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import static java.lang.Math.max;

public class ComposerEvaluator {
	private SortedSet<Similarity> comparators;
	private static final Logger log = Logger.getLogger(ComposerEvaluator.class);
	private ExecutorService rankingCalculators;
	private ExecutorService distanceCalculators;
	private int numMappersPerReducer;

	public ComposerEvaluator(SortedSet<Similarity> comparators) {
		this.comparators = comparators;
		int numThreads = Props.getProps().getInt("numThreads", 1);
		int numReducers = (int) Math.ceil(numThreads / 5.0);
		numReducers = max(numReducers, 1);
		int numMappers = numThreads - numReducers;
		numMappers = max(numMappers, 1);
		//total number of threads is numComposers*(sum of the two values above)
		log.info("Using " + numMappers + " mapper threads");
		log.info("Using " + numReducers + " reducer threads");
		this.numMappersPerReducer = numMappers / numReducers;
		rankingCalculators = Executors.newFixedThreadPool(numReducers, new ThreadFactory() {

			private AtomicInteger threadNumber = new AtomicInteger(0);

			public Thread newThread(Runnable r) {

				Thread t = new Thread(new ThreadGroup("reducers"), r, "reducer-" + threadNumber.getAndIncrement());
				if (t.isDaemon()) {
					t.setDaemon(false);
				}
				if (t.getPriority() != Thread.MAX_PRIORITY) {
					t.setPriority(Thread.MAX_PRIORITY);
				}
				return t;
			}
		});
		distanceCalculators = Executors.newFixedThreadPool(numMappers, new ThreadFactory() {

			private AtomicInteger threadNumber = new AtomicInteger(0);

			public Thread newThread(Runnable r) {
				Thread t = new Thread(new ThreadGroup("mappers"), r, "mapper-" + threadNumber.getAndIncrement());
				if (t.isDaemon()) {
					t.setDaemon(false);
				}
				if (t.getPriority() != Thread.MAX_PRIORITY) {
					t.setPriority(Thread.MAX_PRIORITY);
				}
				return t;
			}
		});
	}

	public void evaluate(BigramComposer c, VectorStore fvm, Partition p) {
		log.info("EVALUATION");
		try {
			if (Props.getProps().getBoolean("pipeline.useComparators"))
				evaluateWithComparators(c, fvm, p);
			if (Props.getProps().getBoolean("pipeline.runNN1a"))
				runNNStudy1_interpretation1(c, fvm, p);//todo remove
			if (Props.getProps().getBoolean("pipeline.runNN1b"))
				runNNStudy1_interpretation2(fvm, p);
			if (Props.getProps().getBoolean("pipeline.runNN2"))
				runNNStudy2(c, fvm, p);
		} catch (IOException e) {
			log.error("Unable to write evaluation results to file");
			e.printStackTrace();
		}

	}

	/**
	 * Evaluates all composers by comparing the similarity of vectors created by
	 * them for a collection of pairs. The bigrams considered have the following
	 * properties
	 * 1. Adjective is one of 36 Baroni adjectives
	 * 2. Noun has been observed in corpus
	 * 3. Bigram has been observed in corpus
	 *
	 * @param fvm vector store, Must contain the observed vectors for all terms
	 *            from the fvm for training
	 */
	public void evaluateWithComparators(BigramComposer composer, VectorStore fvm, Partition p) throws IOException {
		BufferedWriter output = null;
		String format = "dd.MM.yyyy'_'HH.mm.ss";
		SimpleDateFormat df = new SimpleDateFormat(format);

		File f = new File(Props.getProps().getString("data.outdir") + "/comparators/");
		f.mkdirs();
		f = new File(f.getAbsolutePath() + "/comparators-" + "fold-" + p.getFoldNumber() + ".csv");

		try {
			output = new BufferedWriter(new FileWriter(f));
		} catch (IOException ex) {
			log.error(ex);
		}
		try {
			output.write("ADJ, NOUN, METRIC, SCORE\n");
			Set<String> interestingAdjectives = BaroniUtils.getInterestingAdjectives();
			for (String bigram : p.getTestingData()) {
				String[] terms = bigram.split(" ");
				// we must have data for both the adjective and noun we want to compose
				//we also need need an observed vector for the bigram to compare the output to
				if (fvm.containsTermPairVector(terms[0], terms[1])
				&& interestingAdjectives.contains(terms[0])
				&& fvm.getNouns().contains(terms[1])) {

					double[] observed = fvm.getTermVector(bigram);//the expected results
					double[] predicted;
					predicted = composer.compose(fvm, terms[0], terms[1]);
					if (predicted != null) {
						for (Similarity e : comparators) {
							double value = e.getSimilarity(predicted, observed);
							output.write(terms[0] + "," + terms[1] + "," + e.getName() + "," + value + "\n");
						}
					} else {
						output.write("FAILED\n");
					}
				}
			}
		} finally {
			output.flush();
			output.close();
		}

	}

	/**
	 * Finds the nearest neighbours in semantic space of a few select adjective clusters
	 * and bigrams. See Baroni and Zamparelli (2010), Section 5. The paper is unclear about
	 * the neighbours of what exactly we search for and in what space. This is the first
	 * interpretation, which makes more sense as an evaluation procedure- we **compose** the
	 * meaning of a bigram and inspect whereabout in feature space it is by looking
	 * at its nearest neighbours in the extended vocabulary (adj, nouns, observed bigrams).
	 */
	private void runNNStudy1_interpretation1(BigramComposer composer, VectorStore fvm, Partition p) throws IOException {
		String[] adjectives = {"American-j", "black-j", "easy-j", "green-j", "historical-j",
		"mental-j", "necessary-j", "nice-j", "young-j"};
		String[] bigrams = {"bad-j luck-n", "electronic-j communication-n"};

		for (String adj : adjectives) {
			log.info("Centroid 1 for: " + adj);
			double[] centroid = new double[fvm.getTermVector(adj).length];//holds centroid of all bigrams of a certain adjective
			Arrays.fill(centroid, 0);
			int processedBigrams = 0;
			for (String bigram : fvm.getPairs()) {
				if (bigram.startsWith(adj)) {
					double[] composed = null;
					try {
						composed = composer.compose(fvm, adj, bigram.split(" ")[1]);
					} catch (IllegalArgumentException ex) {
						continue;
					}
					VectorTools.normalize(composed);

					//compute the centroid online
					for (int i = 0; i < centroid.length; i++) {
						centroid[i] *= processedBigrams;
						centroid[i] += composed[i];
						centroid[i] /= (processedBigrams + 1);
					}
					processedBigrams++;
				}
			}
			printNN("composed-" + adj, "1a", centroid, fvm, p);
		}
	}

	/**
	 * This is the second interpretation of the nearest-neighbour study. We look at the
	 * nearest neighbours of the **observed** bigram vector to gain an intuition about
	 * the behaviour of the adjective as a transformation of the meaning (position) of
	 * the noun.
	 * If this is the correct interpretation, then this study cannot be used to reason
	 * about the accuracy of my replication of Baroni's method, since it operates on the
	 * raw data **before** any learning of adjective matrices is carried out.
	 * Ideally, both NN studies should be go. We are interested in whether the application
	 * of the adjective to the noun positions the noun closer to where we want it to be-
	 * i.e. closer to the observed bigram.
	 * See project wiki for a more detailed discussion.
	 */
	private void runNNStudy1_interpretation2(VectorStore fvm, Partition p) throws IOException {
		String[] adjectives = {"American-j", "black-j", "easy-j", "green-j", "historical-j",
		"mental-j", "necessary-j", "nice-j", "young-j"};
		String[] bigrams = {"bad-j luck-n", "electronic-j communication-n"};

		for (String adj : adjectives) {
			log.info("Centroid 2 for: " + adj);
			double[] centroid = new double[fvm.getTermVector(adj).length];//holds centroid of all bigrams of a certain adjective
			Arrays.fill(centroid, 0);
			int processedBigrams = 0;
			for (String bigram : fvm.getPairs()) {
				if (bigram.startsWith(adj)) {
					double[] observed = fvm.getTermVector(bigram);
					VectorTools.normalize(observed);

					//compute the centroid online
					for (int i = 0; i < centroid.length; i++) {
						centroid[i] *= processedBigrams;
						centroid[i] += observed[i];
						centroid[i] /= (processedBigrams + 1);
					}
					processedBigrams++;
				}
			}
			printNN("observed-" + adj, "1b", centroid, fvm, p);
		}
	}

	/**
	 * Searches and prints the nearest neighbours of a word (represented as both
	 * a string and a feature vector) among all terms in the specified feature vector matrix
	 *
	 * @param word
	 * @param wordVector
	 * @param fvm
	 */
	private void printNN(String word, String expIdentifier, final double[] wordVector, final VectorStore fvm, Partition p) {
		log.info("Finding NN for " + word);
		//compute distances to all
		PriorityQueue<String> q = new PriorityQueue<String>(fvm.getAllTerms().size(), new Comparator<String>() {

			public int compare(String o1, String o2) {
				double[] v1 = fvm.getTermVector(o1);
				double[] v2 = fvm.getTermVector(o2);
				double cos1 = VectorTools.cosine(v1, wordVector);
				double cos2 = VectorTools.cosine(v2, wordVector);
				return cos1 > cos2 ? -1 : 1;
			}
		});
		q.addAll(fvm.getAllTerms());

		File f = new File(Props.getProps().getString("data.outdir") + "/nn" + expIdentifier);
		f.mkdirs();
		f = new File(f.getAbsolutePath() + "/" + word + "-fold-" + p.getFoldNumber() + ".txt");

		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(f));
			for (int i = 0; i < 30; i++) {
				String nn = q.poll();
				out.write(nn + " : " + VectorTools.cosine(wordVector, fvm.getTermVector(nn)));
				out.newLine();
			}
		} catch (IOException ex) {
			log.error("Could not write NN file");
			ex.printStackTrace();
		} finally {
			try {
				out.close();
			} catch (IOException ex) {
				log.error("Could not close stream");
			}
		}
	}

	/**
	 * Study 2 (Section 6) of Baroni and Zamparelli (2010)
	 *
	 * @throws IOException
	 */
	public List<NearestNeighbourResult> runNNStudy2(BigramComposer composer, VectorStore fvm, Partition p) throws IOException {
		log.info("Running NN study 2 on partition " + p.getProgress());

		Set<String> interestingBigrams = BaroniUtils.getInterestingBigrams();

		File outFile = new File(Props.getProps().getString("data.outdir") + "/nn2/");
		outFile.mkdirs();
		outFile = new File(outFile.getAbsolutePath() + "/ranks-fold-" + p.getFoldNumber() + ".csv");


		int toSubmit = CollectionUtils.intersection(interestingBigrams, p.getTestingData()).size();
		CountDownLatch rankingLatch = new CountDownLatch(toSubmit);

		int bigramNumber = 0;
		List<NearestNeighbourResult> results = Collections.synchronizedList(new LinkedList<NearestNeighbourResult>());
		for (String testBigram : p.getTestingData()) {
			String testBigram1 = testBigram.replace("_", " ");
			if (!interestingBigrams.contains(testBigram)) {
				continue;
			} // only bigrams that we have not trained on, but are on Baroni's list

			//give each control thread its own composer, so that many r instances can exist
			rankingCalculators.submit(new NNStudyControlANDReduceWorker(testBigram1, composer,
			fvm, distanceCalculators, numMappersPerReducer, rankingLatch, bigramNumber, results));
			bigramNumber++;
		}

		BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
		writer.write("composer, adj, noun, predictedNN, simScore, rank\n");//header
		try {
//            assert bigramNumber == interestingBigrams.size();
			log.trace("Running NN study 2 on bigrams: " + bigramNumber);
//            rankingCalculators.shutdown();
			rankingLatch.await();

			for (NearestNeighbourResult result : results) {
				writer.write(result.toString());
				writer.write("\n");
			}

			log.info("Awaiting NN study threads");
		} catch (InterruptedException ex) {
			log.error("Interrupted");
		} finally {
			writer.flush();
			writer.close();
		}
//        if (composer instanceof BaroniComposer) {
//            for (int i = 0; i < composers.length; i++) {
//                BaroniComposer b = (BaroniComposer) composers[i];
//                b.stopR();//ensure no threads are left alive
//            }
//        }
		log.info("Done with NN study");
		return results;
	}

	public void shutdown() {
		distanceCalculators.shutdown();
		rankingCalculators.shutdown();
	}

}
