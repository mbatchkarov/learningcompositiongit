/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras;

import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.Collection;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

public class Partition implements Serializable {

	private static final Logger log = Logger.getLogger(Partition.class);

	/* training/testing strings */
	private SortedSet<String> trainingData, testingData;
	/* ID of this partition and total number of partitions, e.g.
			 1 out of 13, 2 out of 13, etc. Used to measure progress*/
	private int number, total;

	public Partition() {
		trainingData = new TreeSet<String>();
		testingData = new TreeSet<String>();
	}

	/**
	 * @param allData     - the entire unmodified data set
	 * @param testingData - a subset of {@code allData} which is to be used for testing.
	 *                    The set difference of allData and testingData will be used for training
	 * @param number      - the id of this partition, between 1 and {@code total}
	 * @param total       - the total number of partitions of the data
	 */
	public Partition(Collection<String> allData, Collection<String> testingData,
	                 int number, int total) {
		this();
		this.number = number;
		this.total = total;
		this.testingData.addAll(testingData);
		this.trainingData.addAll(allData);
		this.trainingData.removeAll(testingData);

		for (String bigram : allData) {
			assert (bigram.split(" ").length == 2);
		}
		for (String test : testingData) {
			assert (!trainingData.contains(test));
		}
	}
	
	public void union(Partition p1){
		addToTestingData(p1.getTestingData());
		addToTrainingData(p1.getTrainingData());
	}

	public void addToTrainingData(Collection<String> data) {
//		log.warn("Adding to training data");
		trainingData.addAll(data);
	}

	public void addToTestingData(Collection<String> data) {
//		log.warn("Adding to testing data");
		testingData.addAll(data);
	}

	public Set<String> getTestingData() {
		return testingData;
	}

	public Set<String> getTrainingData() {
		return trainingData;
	}

	public void setTrainingData(Set<String> data) {
		log.warn("Modifying training data");
		this.trainingData.addAll(data);
	}

	public void setTestingData(Set<String> data) {
		log.warn("Modifying training data");
		this.testingData.addAll(data);
	}

	/**
	 * Returns a progress string, identifying this partition, e.g.
	 * "1/20", "2/20", etc. Read these as "partition number 1 out of a total of 20"
	 *
	 * @return
	 */
	public String getProgress() {
		return number + "/" + total;
	}

	public int getFoldNumber() {
		return number;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Partition partition = (Partition) o;

		if (!testingData.equals(partition.testingData)) return false;
		if (!trainingData.equals(partition.trainingData)) return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = trainingData.hashCode();
		result = 31 * result + testingData.hashCode();
		return result;
	}

	@Override
	public String toString() {
		return "Partition{" +
		trainingData +
		testingData +
		'}';
	}
}
