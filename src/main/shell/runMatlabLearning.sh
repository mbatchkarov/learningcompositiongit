#!/bin/sh

PATH=$PATH:/opt/local/bin/:/Applications/Matlab/MATLAB_R2011b.app/bin/:/cm/shared/apps/MATLAB/R2011a/bin/matlab

echo Log will be: logs/learning_$3.log > logs/learning_$3.log
echo Shell parameters are: $@ >> logs/learning_$3.log
echo Path is: $PATH >> logs/learning_$3.log
echo Matlab executable is : `which matlab` >> logs/learning_$3.log

X="	cd src/main/matlab; trainBaroniComposer('${1}', '${2}', '${3}', ${4}, ${5})"
echo ${X} > matlab_command.m
cat matlab_command.m
matlab -nojvm -nodisplay -nodesktop -nosplash -noawt < matlab_command.m >>logs/learning_$3.log 2>>logs/learning_$3.log
rm matlab_command.m