#!/bin/bash

# in-file out-prefix adjective-string(=log-file-prefix) num-dimensions num-PLSR-components
echo R process running in directory `pwd` >> logs/learning_$3.log

echo Log will be: logs/learning_$3.log > logs/learning_$3.log
echo Shell parameters are: $@ >> logs/learning_$3.log
echo Path is: $PATH >> logs/learning_$3.log
echo Matlab executable is : `which matlab` >> logs/learning_$3.log
ls  >> logs/learning_$3.log

# R --slave --args $1 $2 $3 $4 $5 < src/main/shell/plsr_adjnoun_dimension_prediction.R >>logs/learning_$3.log 2>>logs/learning_$3.log	
R --vanilla < src/main/shell/plsr_adjnoun_dimension_prediction.R --args $1 $2 $3 $4 $5  >>logs/learning_$3.log 2>>logs/learning_$3.log	

# line below work on 2D test data
# r --vanilla <plsr_adjnoun_dimension_prediction.R --args outputEva/dataset2/ivs-dvs.txt outputEva/beta adj-j 2 2 >outputEva/outmiro.txt 2>outputEva/errmiro.txt