"""
Writes a file containing a 2d data set before and after a known transformation is applied.
Meant to test if PLSR recovers the transformation matrix with and without noise
"""

__author__ = 'mmb28'
from scipy import *
from numpy import *


def generate(noise=False):
#    before = rand(100,2)*[1,0]
    x = rand(100,1)+3
    y = rand(100,1)*.2
    z = rand(100,1)*.6-1
    before = hstack((x,y,z))
#    print 'before',(before)

    rot = [ [.1, 3, 6], [3, 1, 2], [4, 5, 0.1]]

    after = before.dot(rot)
    if noise:
        s = after.shape
        n = rand(s[0],s[1])*[0, 0.005]
        after += n
    print 'after',(after)
#    f = figure()
#    scatter(before[:,0], before[:,1], 'g')
#    scatter(after[:,0], after[:,1], 'g')
#    savefig("figure.png", dpi=300, format="png")



    return before, after

def format_me(before, after):
    s = []
    s.append('%s\t%1.5f\t%1.5f\t%1.5f'%('adj-j', 10, 10, 10))
    for i in range(before.shape[0]):
        s.append('%s%d-n\t%1.5f\t%1.5f\t%1.5f'%('noun',i, before[i,0], before[i,1], before[i,2]))
        s.append('%s%d-n\t%1.5f\t%1.5f\t%1.5f'%('adj-j_noun',i, after[i,0], after[i,1], after[i,2]))
    return s

def format_matrix(matrix):
    s = []
    for i in range(matrix.shape[0]):
        s.append('%1.5f\t%1.5f\t%1.5f'%(matrix[i,0], matrix[i,1], matrix[i,2]))
    return s

suffix = 4
before, after = generate()
with open('plsr_training%s.txt'%suffix, 'w') as out:
    for line in format_me(before, after):
        print line
        out.write(line)
        out.write('\n')

with open('plsr_seen_data%s.txt'%suffix, 'w') as out:
    for line in format_matrix(before):
        out.write(line)
        out.write('\n')

with open('plsr_seen_targets%s.txt'%suffix, 'w') as out:
    for line in format_matrix(after):
        out.write(line)
        out.write('\n')



before, after = generate()

with open('plsr_unseen_data%s.txt'%suffix, 'w') as out:
    for line in format_matrix(before):
        out.write(line)
        out.write('\n')


with open('plsr_unseen_targets%s.txt'%suffix, 'w') as out:
    for line in format_matrix(after):
        out.write(line)
        out.write('\n')
