from itertools import product
from random import random

__author__ = 'mmb28'

with open("evaluator.test.data.txt", 'w') as out:
    for num in range(30):
        out.write('%s-j\t1\t%d\n' % (num, num))
        out.write('%s-n\t1\t%d\n' % (num, num))

# generate random 2D vectors with elements between 10 and 20         
    for num1, num2 in product(range(30), range(30)):
    	x = 20 + 30*random()
    	y = 20 + 30*random()
    	print x, y
        out.write('%s-j_%s-n\t%f\t%f\n' % (num1, num2, x, y))