#usage: python plotComposerResults.py <dir> <composerName>
#processes all files in <dir> whose name starts with "ranks" and considers
#lines containing <composerName> only- e.g. "baroni", "algebra", etc

from collections import defaultdict
import os
import re
from matplotlib.pyplot import *
from numpy import  median
from numpy.lib.function_base import percentile

print(os.getcwd())
directory = '../../../../data/experiment%s/output/nn2'%(sys.argv[1])
print(directory)
outdir = os.path.join(directory, 'images')
if not os.path.exists(outdir):
    os.mkdir(outdir)
desiredComposer = str(sys.argv[2]).lower()

ranks = defaultdict(list) #map from adjective to rank of predicted nearest neighbour
folds = defaultdict(int) #map from adjective to num folds for it
all = list() #the ranks of all points in the test set, concatatenation of all values in ranks dict

files = [file for file in os.listdir(directory) if str(file).startswith('ranks')]
for file in files:
    for line in open(os.path.join(directory, file), 'r'):
        line = re.sub('\[.+\]','',line)#remove excessive information that might have been printed by composer
        composer, adj, noun, predictedNN, similarity, rank = line.split(',')

        #ignore any other composers that happen to be there
        if desiredComposer not in str(composer).lower():
            continue
        ranks[adj].append(int(rank))
        all.append(int(rank))

os.chdir(outdir)
for adj, ranksList in ranks.iteritems():
    infoString = 'ADJ:%s, FOLDS:%d, MEDIAN:%d' % (adj, len(files), median(ranksList))
    print(infoString)#, percentile(ranksList, 50)

    f = figure()
    plot(sorted(ranksList), 'bo')
    #    axhline(percentile(ranksList, 25))
    #    axhline(percentile(ranksList, 50))
    #    axhline(percentile(ranksList, 75))

    figtext(0.4, 0.15, 'Quartiles: %d, %d, %d' % (percentile(ranksList, 25),
                                                  percentile(ranksList, 50), percentile(ranksList, 75)),
        horizontalalignment='center',
        verticalalignment='bottom')

    ylim(0, 1000)
    title(infoString)
    xlabel('Bigram id')
    ylabel('Rank of predicted NN')
    savefig('%s-RANKS.png' % adj, format='png')
print("median across all adjectives", percentile(all, 50))
with open('summary.txt', 'w') as outFile:
    outFile.write('Quartiles across all adjectives: %d, %d, %d' % (percentile(all, 25),
                                             percentile(all, 50), percentile(all, 75)))
print('done')
