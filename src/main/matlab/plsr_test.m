%%generate a few data points in 2d, rotate them- a transformation within
%the same space
close all; clear all; 
data = [rand(120,1)-21 rand(120,1)*2];
myx = data(1:90,:);
test = data(90:end,:);
% rot = [ 0 -1; 1 0];
% rot = [ -1 5; 2 7];

rot = [ [-1, 21]; [.1, 7]];

myy = myx*rot;
%%
%plot values before noise is added
scatter(myx(:,1),myx(:,2), 'go'); hold on;
scatter(myy(:,1),myy(:,2), 'ro');

%add some noise to the desired output
% myy = myy + randn(size(myy))/2;
scatter(myy(:,1),myy(:,2), 'bo');

%learn transformation
[Xloadings,Yloadings,Xscores,Yscores,beta] = plsregress(myx, myy, 2);
beta

yfit = [ones(size(myx,1),1) myx]*beta;
scatter(yfit(:,1),yfit(:,2), 'rx'); 
title('Training data')
legend('before', 'after (no noise)', 'after (w noise)', 'predicted after')

trresiduals = myy-yfit;
% figure;
% stem(trresiduals)
% xlabel('Tr Observation');
% ylabel('Tr Residual');

yfit = [ones(size(test,1),1) test]*beta;

figure
scatter(test(:,1),test(:,2), 'go'); hold on;
trueY = test*rot;
scatter(trueY(:,1),trueY(:,2), 'ro');
scatter(yfit(:,1),yfit(:,2), 'rx'); 
title('Test data')
legend('before', 'true after', 'predicted after')

residuals = trueY-yfit;
% figure;
% stem(residuals)
% xlabel('Test Observation');
% ylabel('Test Residual');

