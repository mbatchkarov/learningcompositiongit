/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras.composer;

import learningalgebras.FeatureVectorMatrix;
import learningalgebras.Partition;
import learningalgebras.reducer.DenseSVD;
import learningalgebras.util.Props;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.ujmp.core.Matrix;
import org.ujmp.core.doublematrix.impl.DefaultDenseDoubleMatrix2D;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CountDownLatch;

import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

public class OneAdjectiveBaroniComposerTest {

	public static final int NUM_FOLDS = 5;
	private FeatureVectorMatrix fvm;

	@Before
	public void setUp() throws Exception {

	}

	@Test
	public void testWriteDataToFile() throws Exception {
		Props.readConfig("./src/test/java/baroni.test1.conf.txt");
		fvm = new FeatureVectorMatrix(Props.getProps().getString("data.file"), new DenseSVD(), 300, 300);
		fvm.newPartitionData(NUM_FOLDS, 321238571);

		Map<String, Integer> timesANounsWasUsedForTraining = new HashMap<String, Integer>();
		for (Partition p : fvm.foldPartitions.values()) {
			CountDownLatch latch = new CountDownLatch(1);
			final String adjWord = "American-j";
			OneAdjectiveBaroniComposer composer = new OneAdjectiveBaroniComposer(adjWord, fvm, p, latch, "Baroni");//todo parameterise the adj and the method
			composer.train(fvm, p);

			String baroniOutputPrefix = Props.getProps().getString("baroni.training.outDir");
			File f = new File(baroniOutputPrefix + adjWord + ".txt");

			BufferedReader reader;
			try {
				reader = new BufferedReader(new FileReader(f));
				String line;
				while ((line = reader.readLine()) != null) {
					String[] input = line.split("\t");
					final String adjective = input[0];
					final String noun = input[1];

					assertTrue(adjective.endsWith("-j"));
					assertTrue(noun.endsWith("-n"));
					Assert.assertEquals(2 * fvm.getVectorDimensionality() + 2, input.length);

					if (timesANounsWasUsedForTraining.containsKey(noun)) {
						timesANounsWasUsedForTraining.put(noun, timesANounsWasUsedForTraining.get(noun) + 1);
					} else {
						timesANounsWasUsedForTraining.put(noun, 1);
					}

					double[] nounVector = parseStringArray(input, 2, 300);
					double[] bigramVector = parseStringArray(input, 302, 300);

					assertArrayEquals(fvm.getTermVector(noun), nounVector, 1e-5);
					assertArrayEquals(fvm.getPairVector(adjective, noun), bigramVector, 1e-5);
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		Set<String> availableNouns = new HashSet<String>();
		for (String bigram : fvm.getPairs()) {
			String[] words = bigram.split(" ");
			if (words[0].equals("American-j"))
				availableNouns.add(words[1]);
		}
		for (String noun : timesANounsWasUsedForTraining.keySet()) {
			int count = timesANounsWasUsedForTraining.get(noun);
			Assert.assertEquals(NUM_FOLDS - 1, count);
		}
		assertEquals(availableNouns, timesANounsWasUsedForTraining.keySet());

//		//expose the private method
//		Method method = OneAdjectiveBaroniComposer.class.getDeclaredMethod("writeDataToFile", VectorStore.class, Partition.class);
//		method.setAccessible(true);
//		method.invoke(composer, fvm, p);
	}

	private double[] parseStringArray(String[] input, int start, int len) {
		double[] toReturn = new double[len];
		for (int i = 0; i < toReturn.length; i++) {
			toReturn[i] = Double.parseDouble(input[start + i]);
		}
		return toReturn;
	}

	@Test
	public void testMatrixMultiplication() throws Exception {
		DefaultDenseDoubleMatrix2D matrix = new DefaultDenseDoubleMatrix2D(2, 2);
		matrix.setAsDouble(1, 0, 0);
		matrix.setAsDouble(2, 0, 1);
		matrix.setAsDouble(3, 1, 0);
		matrix.setAsDouble(4, 1, 1);

		DefaultDenseDoubleMatrix2D unit = new DefaultDenseDoubleMatrix2D(2, 2);
		unit.setAsDouble(1, 0, 0);
		unit.setAsDouble(1, 0, 1);
		unit.setAsDouble(1, 1, 0);
		unit.setAsDouble(1, 1, 1);
		System.out.println("Mtimes-- matrix multiplication");
		Matrix res = unit.mtimes(matrix);
		System.out.println(res);
		assertArrayEquals(res.toDoubleArray()[0], new double[] {4., 6.}, 1e-10);
		assertArrayEquals(res.toDoubleArray()[1], new double[] {4., 6.}, 1e-10);
		System.out.println("times-- pointwise multiplication");
		res = unit.times(matrix);
		assertArrayEquals(res.toDoubleArray()[0], new double[] {1., 2.}, 1e-10);
		assertArrayEquals(res.toDoubleArray()[1], new double[] {3., 4.}, 1e-10);
		System.out.println(res);
	}
}
