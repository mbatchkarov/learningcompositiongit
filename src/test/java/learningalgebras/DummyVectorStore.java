/******************************************************************************
 * Copyright (c) 2012 University of Sussex                                    *
 ******************************************************************************/

package learningalgebras;

import java.util.*;

public class DummyVectorStore implements VectorStore {

	private SortedMap<String, double[]> map;
	private SortedSet<String> set;

	public DummyVectorStore() {
		this.map = new TreeMap<String, double[]>();
		this.set = new TreeSet<String>();
	}

	public void add(String word, double[] vector) {
		set.add(word);
		map.put(word, vector);
	}

	@Override
	public double[] getPairVector(String term1, String term2) {
		return getTermVector(term1 + " " + term2);
	}

	@Override
	public boolean containsTermPairVector(String term1, String term2) {
		return set.contains(term1 + " " + term2);
	}

	@Override
	public double[] getTermVector(String term) {
		return map.get(term);
	}

	@Override
	public int getVectorDimensionality() {
		return map.get(set.toArray()[0]).length;
	}

	@Override
	public SortedSet<String> getAdjs() {
		throw new IllegalAccessError("not implemented");
	}

	@Override
	public SortedSet<String> getNouns() {
		throw new IllegalAccessError("not implemented");
	}

	@Override
	public SortedSet<String> getTerms() {
		return set;
	}

	@Override
	public SortedSet<String> getPairs() {
		throw new IllegalAccessError("not implemented");
	}

	@Override
	public SortedSet<String> getAllTerms() {
		return Collections.unmodifiableSortedSet(new TreeSet<String>(map.keySet()));
	}
}
