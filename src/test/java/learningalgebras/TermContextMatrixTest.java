/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package learningalgebras;

import org.junit.*;
import org.ujmp.core.Matrix;

import java.util.Map;

import static junit.framework.Assert.assertEquals;

/**
 *
 * @author bc73
 */
public class TermContextMatrixTest {

    public TermContextMatrixTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of constructor of class TermContextMatrix.
     */
    @Ignore
    @Test //TODO this test fails, but term context matrices are no longer used
    public void testBuild() throws Exception {
        String file = "./src/test/java/phrase_test.txt";
        TermContextMatrix tcm = new TermContextMatrix(file, 2, 2, 2, 2, 0);
        Map<String, Integer> termMap = tcm.getTermMap();
        assertEquals(2, termMap.size());
        assertEquals(2, tcm.getContextMap().size());
        assertEquals(2, tcm.getAdjSet().size());
        assertEquals(2, tcm.getNounSet().size());
        Matrix m = tcm.getMatrix();
        int v1 = tcm.getContextMap().get("v1");
        assertEquals(0, m.getAsInt(termMap.get("a1"), v1));
        assertEquals(0, m.getAsInt(termMap.get("a2"), v1));
        assertEquals(1, m.getAsInt(termMap.get("n1"), v1));
        assertEquals(1, m.getAsInt(termMap.get("n2"), v1));
        assertEquals(1, m.getAsInt(termMap.get("a1 n1"), v1));
        assertEquals(1, m.getAsInt(termMap.get("a2 n2"), v1));
        assertEquals(0, m.getAsInt(termMap.get("a1 a2"), v1));
        int n2 = tcm.getContextMap().get("n2");
        assertEquals(1, m.getAsInt(termMap.get("a1"), n2));
        assertEquals(2, m.getAsInt(termMap.get("a2"), n2));
        assertEquals(0, m.getAsInt(termMap.get("n1"), n2));
        assertEquals(0, m.getAsInt(termMap.get("n2"), n2));
        assertEquals(0, m.getAsInt(termMap.get("a1 n1"), n2));
        assertEquals(0, m.getAsInt(termMap.get("a2 n2"), n2));
        assertEquals(1, m.getAsInt(termMap.get("a1 a2"), n2));
    }
}