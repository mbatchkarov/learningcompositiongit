/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras.util;

import junitx.framework.ArrayAssert;
import java.util.ArrayList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author bc73
 */
public class CollectionUtilsTest {

    public CollectionUtilsTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of partitionList method, of class CollectionUtils.
     */
    @Test
    public void testPartitionList() {
        for (int n = 1; n <= 10; n++) {
            List<Integer> l = new ArrayList<Integer>();
            for (int i = 0; i < n; i++) {
                l.add(n);
            }
            for (int i = 1; i <= n; i++) {
                List<List<Integer>> p = CollectionUtils.partitionList(l, i);
                assertEquals(i, p.size());
                int elements = 0;
                for (List<Integer> sublist : p) {
                    elements += sublist.size();
                }
                assertEquals(elements, n);
            }
        }
    }

    @Test
    public void testOtherVectorUtilsMethods() {
        double[] word1 = {.1, .2, .3, .4, .5, .6};
        double[] word2 = {-0.6, .5, .4, .3, .2, .1};

        //sanity check
        ArrayAssert.assertEquals(word1, word1, 0.000000001);

        //cosine 
        assertEquals(VectorTools.cosine(word1, word1), 1.0, 0.0001);
        assertEquals(VectorTools.cosine(word1, word2), 0.4835, 0.0001);
        assertEquals(VectorTools.cosine(word2, word2), 1.0, 0.0001);

        //l1 norm
        assertEquals(VectorTools.l1Norm(word1), 2.1, 0.0001);
        assertEquals(VectorTools.l1Norm(word2), 2.1, 0.0001);

        //l2 norm
        assertEquals(VectorTools.l2Norm(word1), 0.9539, 0.0001);
        assertEquals(VectorTools.l2Norm(word2), 0.9539, 0.0001);

        //normalization
        double[] normalized1 = VectorTools.unitVector(word1);
        double[] normalized2 = VectorTools.unitVector(word2);
        assertEquals(1.0, VectorTools.l2Norm(normalized1), 0.000001);
        assertEquals(1.0, VectorTools.l2Norm(normalized2), 0.000001);


        double[] word1Backup = {.1, .2, .3, .4, .5, .6};
        ArrayAssert.assertEquals(word1, word1Backup, 0.000000001);

        VectorTools.normalize(word1);
        assertEquals(VectorTools.l2Norm(word1), 1.0, 0.0001);

        assertEquals(VectorTools.cosine(word1, word1Backup), 1.0, 0.00001);
    }
}
