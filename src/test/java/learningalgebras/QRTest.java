package learningalgebras;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ojalgo.matrix.decomposition.QR;
import org.ojalgo.matrix.decomposition.QRDecomposition;
import org.ujmp.core.Matrix;
import org.ujmp.core.MatrixFactory;
import org.ujmp.ojalgo.OjalgoDenseDoubleMatrix2D;
import static org.junit.Assert.*;

/**
 *
 * @author bc73
 */
public class QRTest {

    private OjalgoDenseDoubleMatrix2D x;
    private OjalgoDenseDoubleMatrix2D a1;
    private double[] b1 = {0.3529, 0.4872, 0.0017, 0.2555, 0.0409};
    private OjalgoDenseDoubleMatrix2D a2;
    private double[] b2 = {0.2158, 0.0585, 0.1527, 0.3939, 0.1621};

    public QRTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        double[][] xVals = {
            {0.8305, 0.4189, 0.2097, 0.8574, 0.7974},
            {0.7063, 0.5911, 0.3028, 0.0494, 0.4475},
            {0.1246, 0.8725, 0.8398, 0.3175, 0.5778},
            {0.9016, 0.4972, 0.2220, 0.1278, 0.7195},
            {0.4163, 0.6454, 0.6714, 0.7835, 0.1616},
            {0.0112, 0.5513, 0.5279, 0.5944, 0.3179},
            {0.0083, 0.8867, 0.4664, 0.0988, 0.6646},
            {0.6662, 0.0056, 0.7697, 0.7620, 0.5506},
            {0.2823, 0.0381, 0.6544, 0.0080, 0.7719},
            {0.3030, 0.2410, 0.6699, 0.2103, 0.3756},
            {0.0450, 0.7403, 0.4153, 0.6965, 0.6391},
            {0.7333, 0.3782, 0.7868, 0.0687, 0.2036},
            {0.5045, 0.1743, 0.7775, 0.4342, 0.6722},
            {0.1791, 0.0295, 0.8717, 0.4561, 0.2154},
            {0.6991, 0.1021, 0.4509, 0.7684, 0.4799},
            {0.3135, 0.1270, 0.4658, 0.9090, 0.0959},
            {0.7085, 0.9416, 0.9870, 0.3058, 0.3066},
            {0.5453, 0.6899, 0.2521, 0.7834, 0.1458},
            {0.8862, 0.0396, 0.1452, 0.1661, 0.2038},
            {0.7051, 0.4231, 0.0042, 0.2038, 0.8359}
        };
        x = new OjalgoDenseDoubleMatrix2D(MatrixFactory.linkToArray(xVals));

        double[] a1Vals = {
            0.6713, 0.9265, 0.9798, 0.9720, 0.5274,
            0.8372, 0.6007, 0.2927, 0.2925, 0.7297,
            0.0896, 0.0770, 0.2423, 0.1861, 0.5935,
            0.5969, 0.2766, 0.9816, 0.7552, 0.0095
        };
        a1 = new OjalgoDenseDoubleMatrix2D(MatrixFactory.linkToArray(a1Vals));

        double[] a2Vals = {
            0.9871, 0.0620, 0.0105, 0.1961, 0.9873,
            0.3410, 0.2032, 0.2101, 0.5524, 0.7362,
            0.4135, 0.1894, 0.3210, 0.6566, 0.4421,
            0.4584, 0.8399, 0.4338, 0.2939, 0.8074
        };
        a2 = new OjalgoDenseDoubleMatrix2D(MatrixFactory.linkToArray(a2Vals));
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testSolution() {
        double delta = 5e-4;
        QR<Double> qr = QRDecomposition.makePrimitive();
        qr.compute(x.getWrappedObject());
        Matrix ans = new OjalgoDenseDoubleMatrix2D(qr.solve(a1.getWrappedObject()));
        assertVectorEquals(b1, ans.transpose().toDoubleArray()[0], delta);
        ans = new OjalgoDenseDoubleMatrix2D(qr.solve(a2.getWrappedObject()));
        assertVectorEquals(b2, ans.transpose().toDoubleArray()[0], delta);
    }

    /**
     * Assert that two double are equal to a certain precision.
     *
     * @param expected expect value
     * @param actual actual value
     * @param delta precision
     */
    private void assertVectorEquals(double[] expected, double[] actual, double delta) {
        if (expected.length != actual.length) {
            fail("array dimensions do not match");
        }
        for (int i = 0; i < expected.length; i++) {
            assertEquals(expected[i], actual[i], delta);
        }
    }
}
