package learningalgebras.reducer;

import java.io.IOException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.ujmp.core.Matrix;
import org.ujmp.core.MatrixFactory;
import static org.junit.Assert.*;

/**
 * Asserts that the absolute values from the SVD reducer are the same values
 * obtained from MATLAB. We only test for absolute value as different SVDs
 * implementations may return different signs for the decompositions.
 *
 * @author bc73
 */
public class DenseSVDTest {

    private Matrix input;

    public DenseSVDTest() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Before
    public void setUp() {
        double[][] values = {
            {1, 0, 0, 1, 0, 0, 0, 0, 0},
            {1, 0, 1, 0, 0, 0, 0, 0, 0},
            {1, 1, 0, 0, 0, 0, 0, 0, 0},
            {0, 1, 1, 0, 1, 0, 0, 0, 0},
            {0, 1, 1, 2, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 1, 0, 0, 1, 0, 0, 0, 0},
            {0, 0, 1, 1, 0, 0, 0, 0, 0},
            {0, 1, 0, 0, 0, 0, 0, 0, 1},
            {0, 0, 0, 0, 0, 1, 1, 1, 0},
            {0, 0, 0, 0, 0, 0, 1, 1, 1},
            {0, 0, 0, 0, 0, 0, 0, 1, 1}
        };
        input = MatrixFactory.importFromArray(values);
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of reduce method, of class SVDReducer. 
     */
    @Test
    public void testReduce() throws IOException {
        double[][] values = {
            {-0.7395, -0.2877},
            {-0.6603, -0.1832},
            {-0.8034, 0.1097},
            {-1.3484, 0.1451},
            {-2.1531, -0.4252},
            {-0.8855, 0.2724},
            {-0.8855, 0.2724},
            {-1.0050, -0.3591},
            {-0.6879, 0.6955},
            {-0.0426, 1.2458},
            {-0.1207, 1.5829},
            {-0.1061, 1.1451}
        };
        Matrix expected = MatrixFactory.importFromArray(values);
        Matrix actual = new DenseSVD().reduce(input, 2);
        for (int i = 0; i < expected.getSize(0); i++) {
            for (int j = 0; j < expected.getSize(1); j++) {
                double absExpected = Math.abs(expected.getAsDouble(i, j));
                double absActual = Math.abs(actual.getAsDouble(i, j));
                assertEquals(absExpected, absActual, 5e-5);
            }
        }
    }
}
