package learningalgebras.eval;

import learningalgebras.FeatureVectorMatrix;
import learningalgebras.Main;
import learningalgebras.Partition;
import learningalgebras.composer.BigramComposer;
import learningalgebras.composer.NoisyOracleComposer;
import learningalgebras.composer.OracleComposer;
import learningalgebras.eval.similarity.Cosine;
import learningalgebras.eval.similarity.Similarity;
import learningalgebras.reducer.DenseSVD;
import learningalgebras.util.BaroniUtils;
import learningalgebras.util.NearestNeighbourResult;
import learningalgebras.util.Props;
import org.junit.Test;

import java.io.IOException;
import java.util.List;
import java.util.TreeSet;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Copyright
 * User: mmb28
 * Date: 13/08/2012
 * Time: 11:03
 */
public class ComposerEvaluatorTest {

	public static final int NUM_ADJECTIVES = 30;
	public static final int DIMENSIONALITY = 2;

	@Test
	public void testRunNNStudy2() throws Exception {
		System.out.println(System.getProperty("user.dir"));
		Props.readConfig("src/test/java/evaluator.test.conf.txt");
		FeatureVectorMatrix fvm = Main.getData(Props.getProps().getString("data.file"), true, false, 50,
		Props.getProps().getInt("reducer.max", DIMENSIONALITY), new DenseSVD(), Props.getProps().getInt("data.dim"));
		assertEquals(DIMENSIONALITY, fvm.getVectorDimensionality());

		BaroniUtils.setInterestingWords(fvm.getAdjs(), fvm.getPairs());

		fvm.newPartitionData(30, 239412387, true);
		ComposerEvaluator evaluator = new ComposerEvaluator(new TreeSet<Similarity>() {{
			add(new Cosine());
		}});
		BigramComposer comp;

		comp = new OracleComposer();
		evaluateComposer(fvm, evaluator, comp, 1);

		int oldSumOfRanks = 0;
		for (int i = 0; i < 2; i += 0.1) {
			comp = new NoisyOracleComposer(i);
			int newRank = evaluateComposer(fvm, evaluator, comp);
			assertTrue(oldSumOfRanks < newRank);
			oldSumOfRanks = newRank;
		}

		System.out.println("Done");

	}

	private void evaluateComposer(FeatureVectorMatrix fvm, ComposerEvaluator evaluator, BigramComposer comp, int expected)
	throws IOException {
		for (Partition partition : fvm.foldPartitions.values()) {
			assertEquals(NUM_ADJECTIVES, partition.getTestingData().size());
			List<NearestNeighbourResult> results = evaluator.runNNStudy2(comp, fvm, partition);
			for (NearestNeighbourResult result : results) {
				assertEquals(expected, result.getRank());
			}
		}
	}

	private int evaluateComposer(FeatureVectorMatrix fvm, ComposerEvaluator evaluator, BigramComposer comp)
	throws IOException {
		int sumOfRanks = 0;
		for (Partition partition : fvm.foldPartitions.values()) {
			assertEquals(NUM_ADJECTIVES, partition.getTestingData().size());
			List<NearestNeighbourResult> results = evaluator.runNNStudy2(comp, fvm, partition);
			for (NearestNeighbourResult result : results) {
				sumOfRanks += result.getRank();
			}
		}
		return sumOfRanks;
	}
}
