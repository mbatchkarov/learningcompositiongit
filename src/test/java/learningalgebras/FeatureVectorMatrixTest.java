/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package learningalgebras;

import com.rits.cloning.Cloner;
import junitx.framework.ArrayAssert;
import learningalgebras.reducer.DenseSVD;
import learningalgebras.reducer.Reducer;
import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;
import org.junit.*;
import org.ujmp.core.Matrix;
import org.ujmp.core.MatrixFactory;

import java.util.*;

import static learningalgebras.util.Props.readConfig;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * @author bc73
 */
public class FeatureVectorMatrixTest {
	private static final Logger log = Logger.getLogger(FeatureVectorMatrixTest.class);
	private FeatureVectorMatrix fvm;

	public FeatureVectorMatrixTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
		readConfig("./src/test/java/baroni.test1.conf.txt");
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@Before
	public void setUp() {
		double[][] values = new double[10][7];
		Matrix m = MatrixFactory.linkToArray(values);
		List<String> unigrams = Arrays.asList("w1", "w2", "w3", "w4", "w5", "w6");
		SortedSet<String> bigrams = new TreeSet<String>(Arrays.asList("w1 w5", "w2 w5", "w1 w6", "w2 w6"));

		SortedSet<String> adjSet = new TreeSet<String>(unigrams.subList(0, 4));
		SortedSet<String> nounSet = new TreeSet<String>(unigrams.subList(4, 6));
		Map<String, Integer> termMap = new HashMap<String, Integer>();
		int index = 0;
		for (String term : unigrams) {
			termMap.put(term, index++);
		}
//		for (String adj : adjSet) {
//			for (String noun : nounSet) {
//				termMap.put(adj + ' ' + noun, index++);
//			}
//		}
		for (String b : bigrams) {
			termMap.put(b, index++);
		}
		assertEquals(index, values.length);
		fvm = new FeatureVectorMatrix(m, adjSet, nounSet, bigrams, termMap);
	}

	@After
	public void tearDown() {
	}

	@Test
	@Ignore
	/**
	 * Test of method parseBaroniMatrixFromFile
	 */
	public void testBaroniMatrixParser() {
		String path = "../data/baroni.matrix.txt";
		Reducer r = new DenseSVD();

		FeatureVectorMatrix fvmBaroni = new FeatureVectorMatrix(path, r, 300, 300);

		double[] v1 = fvmBaroni.getTermVector("American-j");
		double[] e1 = {36328.3594, -12810.8188, 32025.2003, -10706.5547, -54144.0587, -1459.7847, -8585.2549, 18533.6042, 71753.7408, -22378.5208, 33743.5466, -12070.4607, 9107.5654, 22188.5873, -5057.1548, -17300.1511, -9062.1936, -54028.9715, -6011.1400, -24919.5004, -11432.2628, 9436.6555, -32087.2737, 710.5013, 74162.8924, 6551.9030, -3512.8786, 56527.6196, -14205.3046, 68627.1467, 25419.7048, 22289.6637, -55468.4432, -8753.4531, 3631.6317, 14565.1729, -18193.5897, 3960.5715, -1564.3013, 17916.3767, -3712.1290, 2596.5246, 9724.3193, 2722.2901, 5764.5183, -2697.2983, 6680.3103, -1635.1714, 10164.4606, 19518.8794, -3461.5985, 10146.5467, 20576.7058, -1700.8318, 272.8873, -9822.3193, -149.6382, -2881.5538, 3653.9847, -2497.1532, -1050.3439, -1347.5670, 5391.0226, 3227.3252, -902.6486, 3720.4828, -693.4448, 8269.2285, 6773.1346, -5988.9162, -786.3914, 1286.7262, -7501.6096, -3945.2636, -3829.4121, -762.6443, -79.7309, 1846.9417, 6004.0983, -8528.5259, 1381.8596, -8418.2886, 5543.7857, 3964.8608, -520.2059, 4901.0336, -4334.8765, 905.7442, 1141.0465, 3541.9202, -2648.7802, 9521.7360, 2771.6423, -2929.0649, -2976.2972, -2223.7052, -8218.7083, -6799.2202, 1477.9978, 163.7572, -504.9945, 3926.0726, 187.3436, -6935.5473, 1780.2561, 9912.2689, -8990.3552, 1811.0484, 5534.7610, -1252.5318, -10758.4564, 1548.5293, 3967.2900, -2604.5681, -3660.6578, 501.2493, -2929.6112, 2177.7408, 3484.2415, -9536.0693, 1790.0230, -313.4577, 7343.9678, 12002.2095, -2051.0611, 2219.1667, -7674.1104, -1146.9266, 7659.3375, 5448.4472, -12471.4787, 6776.6139, 9344.8747, -1191.3292, 1333.2902, -7169.0290, 2011.2699, 10950.5935, 3538.4559, 5668.9736, 163.1971, -1170.0291, -1994.6577, -1629.7318, 5672.8968, 4853.3587, -1230.6419, 4070.7114, 7564.1313, 6466.2177, -993.8353, -5065.8154, -1984.8163, -2617.8130, 1867.4843, 29.4566, 1579.5058, -7480.2684, 3477.5361, -6826.9019, -6396.0523, 3556.8748, -11254.3577, -2371.1548, 3258.8594, 185.9375, 1085.9592, -51.3872, 9416.3694, -2697.3807, 4181.4762, 5781.5363, -6066.2431, -6862.1114, -9698.0528, -8877.7061, 2515.4637, -8178.2219, -1171.3810, -926.8119, -16429.9911, 7756.2245, 19921.8206, 6082.4257, 412.8514, 1033.6250, 1822.7609, 21202.7128, -19608.8953, 11772.0243, 10999.5306, 20428.3115, 394.0735, 17152.4145, 3937.9173, -8344.0810, 2906.1053, 3403.9031, 2561.6118, -9912.7328, 298.4777, -4364.6983, -13003.3229, -5861.9970, -418.5561, 1273.5040, -6353.1996, -762.5304, -2226.5437, -6175.3235, 8599.3698, 1254.4790, -3576.6965, -1494.0943, 7244.3856, -2853.1767, -8163.6967, 3659.4504, 5859.2996, 7396.6528, 5779.7912, -3215.6130, -1550.3190, 5337.1778, 4155.7571, 7428.7380, -7796.5532, -5161.5488, 7544.4024, -14335.0817, -803.4662, 5989.3854, -7359.3866, -9851.3316, -980.3815, -333.9808, -4360.4233, 3033.6252, 2939.5083, -5105.8432, -4147.4407, 1111.8546, -1815.2627, 2708.8639, 6421.3193, 2241.5105, -1281.7003, 6394.6273, 111.7864, -4181.5325, 6.7313, 4147.1015, -2767.1571, 3082.9532, -1416.6774, -2177.1333, -3377.3274, -1454.3757, -1265.1114, -412.0459, -342.4794, -825.1288, -1015.6012, -414.6496, 2485.9980, -5239.5008, 3497.0293, 2535.1030, 2626.3655, 2740.4205, -1995.5190, -1476.6317, -1097.3165, -3641.5440, -2300.1186, 2742.5213, 533.7759, -530.8887, 1031.5258, 764.1330, 2169.6888, -1332.5365, -3992.6666, 19.4300, -2398.4043, -2319.9697, -757.4220, -495.7340, 25.4253, -2201.0886, 896.4528, 760.9461, 31.8583, 2125.9022, 1659.3132, -385.5961, -1711.4986, -3659.8234, 3612.7572, 1258.3260};

		double[] v2 = fvmBaroni.getTermVector("American-n");
		double[] e2 = {1345.7020, -441.7035, 1084.7811, -22.2431, 510.7364, 66.2904, -264.0862, -171.1379, 858.7546, -332.4721, 412.2185, 513.1991, -185.3291, -431.7329, -309.3253, -712.5053, 431.8851, -97.1572, 468.2078, -217.8617, -105.1083, 56.3273, -468.4439, -57.4255, 1158.1356, 430.3705, -272.4728, -416.5463, -385.5265, -52.0420, 515.3342, -361.5424, 34.9038, 16.2039, 145.7302, 272.0581, -89.8009, 1006.1030, -312.4650, -119.7578, 627.4781, 279.4272, -22.4644, -68.3218, 491.5916, -215.4591, -294.6427, 13.2502, -79.6151, 0.4451, -60.4135, 403.2379, 319.3936, 64.3708, -6.7481, -298.8449, 494.6282, -172.5764, -84.0607, -155.1923, -124.7311, -18.6804, 165.1866, -80.2192, -29.8962, -70.8051, 381.1622, -81.3484, -127.3659, -125.3540, 344.1672, -34.2781, -68.8737, -297.4557, -289.0675, 114.8051, 343.0190, -176.8029, 204.1494, -23.0233, 46.4895, -38.7070, 206.4194, -38.1365, 36.5162, -32.4820, -317.4110, 361.7123, -443.6252, 286.0927, -237.0865, 300.7773, -9.0479, -722.3436, -91.9721, 49.6800, -421.6612, 503.8489, 78.5752, -37.9321, 99.9748, 406.0453, -243.1113, -206.2011, -5.5732, 632.0196, 116.9384, -1.2260, -89.2781, -169.6128, 87.3850, 132.3043, 65.4019, -77.0963, 269.7094, -11.8831, -130.0238, 242.2136, 406.9729, 357.3357, 549.6656, 704.5373, 612.6001, 196.2838, 192.2014, -382.7219, -287.9464, 148.5562, 372.4780, 24.9610, -543.6277, 400.9912, 650.8809, 457.2832, 210.0252, -477.8600, 646.9739, -187.4916, 628.2961, 474.1649, 25.8421, 42.0560, -57.4905, 560.8595, 130.0858, 485.2830, -217.6619, -522.1041, -258.0943, -601.4870, 540.1711, -656.1358, 110.8882, 362.7564, 344.8312, -173.6643, -640.3882, -250.1574, 14.7913, 365.8246, 13.6198, -269.5110, -194.0957, -760.7240, -186.5529, 278.6912, 75.4339, -6.0358, -150.7726, 402.8931, 274.4662, 352.4478, 358.7751, -60.4440, -20.4162, -52.9136, 62.7775, -226.6107, 204.4533, -82.8232, -121.1015, -43.9466, 7.4628, 86.9115, -107.3086, 221.9868, -72.5884, 358.8558, -539.1621, -137.3974, 252.7729, 454.3791, -55.1114, 438.3981, 76.7948, -136.4385, 194.8972, 283.3331, 47.8419, -461.4068, 331.3841, -157.1700, -402.0430, -243.1058, 316.9982, -213.6734, -25.0257, 389.0738, -277.7580, 943.6034, -538.8105, 598.0687, 175.9060, -449.4863, -572.0362, -230.8054, 841.2655, -371.1077, -248.6554, -446.3123, -260.8191, 250.8539, 331.4130, -313.2555, -280.2732, -43.1571, 373.9734, 96.3464, -279.0120, 255.1654, 145.2034, -6.4470, 42.2010, 17.7508, 1.9654, -195.6412, -98.0421, -58.9013, -24.7631, 344.4744, 237.2198, 13.6243, 97.7501, -242.5359, -20.0010, 112.2508, 347.5192, -152.4984, 191.8098, 179.9068, -155.5770, 58.4517, 93.2334, 4.7327, 83.5786, 44.7173, -308.7860, 81.4302, 26.2113, 26.1575, -121.9617, 49.9691, 112.6153, -73.4022, 36.8491, -36.9861, -32.2147, 137.6376, -81.7915, 113.3877, 42.4630, 33.0258, 168.1549, 87.6394, 100.4187, -79.0597, 79.8488, -341.3058, 42.9688, 78.7420, -14.8452, 6.0516, 207.3877, -25.8126, -4.8603, -179.7519, -195.5856, -100.8153, 95.5010, 56.5519, -2.0100, -192.7520, -64.6928, 208.4672, 85.0537, 36.7048, -226.3297, 69.9394, 2.5360, -195.9570};

		double[] v3 = fvmBaroni.getPairVector("general-j", "regulation-n");
		double[] e3 = {21.3483, -21.2133, -5.7787, 3.8167, 5.1725, -5.2317, 20.6143, 11.6568, -2.0472, -4.3064, 11.5309, -4.7112, -0.9593, 4.8120, -4.7200, -13.8072, 3.2014, 11.8025, 18.3222, -19.6632, 6.1380, -9.0883, -2.2939, -5.1909, -11.9461, -11.6120, 0.9435, -7.4760, -13.5611, 8.8836, -8.7655, 9.0272, -8.3358, -3.6290, -2.6291, -3.7029, 4.3565, -7.8358, -5.4284, -3.1531, -17.0873, 2.7873, -22.0789, 18.1781, -0.9246, 2.4667, 0.9706, -1.3375, 8.1058, -5.2179, -2.5827, -1.7275, -5.1459, 9.5253, 6.3726, 5.9936, 4.6093, 4.1031, 7.9892, -1.2462, 6.4335, 3.8222, 8.4893, 4.1265, 1.5740, -0.4045, 3.1704, 0.6313, 3.5290, -12.5487, -7.9857, 2.7618, 10.3036, -2.5688, 10.5934, -6.3347, 1.8415, -1.7720, 3.1254, -0.8525, 11.7454, 1.9797, 3.6747, -1.0525, -3.7971, -0.3313, 1.6590, 2.3328, -1.4154, -2.8970, 0.2576, 0.5910, 3.4836, -0.6575, 0.2251, -2.1670, -4.0199, 4.4173, -3.0191, -0.0094, -1.3060, 3.1303, 0.7690, -2.8960, 6.7605, -2.8190, -2.1806, 1.6359, 7.6905, -1.9281, -5.7742, -4.5948, 6.9529, -5.8320, 4.3723, -11.1699, -2.8946, 8.6489, 1.2280, 2.8974, -0.8550, -8.3924, 3.1507, 2.2023, 7.4134, -3.2890, 1.0656, -0.7537, -2.6501, 0.8793, 2.1281, -2.9092, -8.3883, -0.1652, 4.0208, -0.0056, 6.2533, 1.3964, 3.8584, -4.9430, -0.8106, -7.0366, -4.1315, 2.5622, 0.7176, 3.8398, 4.2092, -1.5318, -4.5141, -5.5485, -1.0172, 3.6419, -0.2406, 0.3395, 10.6103, -1.1574, 4.1046, -2.5825, -5.4760, 5.1770, 8.4353, 3.3332, -3.4677, -3.7505, -5.4183, -10.9680, 0.9586, -1.2560, -0.4296, -3.8656, -1.9346, 3.4054, -0.5631, -0.2677, 3.5888, -6.9097, 3.6331, -2.2860, 1.6980, 5.4194, 2.0568, -2.5207, -0.0956, 0.1224, -4.4928, -1.0652, -0.1158, 3.6430, -1.4155, 1.4781, -6.3685, -2.3391, -1.0371, -1.3198, -3.8383, 1.1384, -0.3131, 1.4094, 1.5019, 4.3022, -4.9170, -4.4464, 1.0663, -5.1347, -2.4849, 1.6246, -5.1659, -1.4626, -6.0467, 1.0725, 1.1153, 3.4874, 0.3757, 1.4492, -0.1326, 5.6073, 4.0877, -1.4493, -2.6962, 0.9747, 5.4290, 0.3306, -1.5768, 2.0289, 3.3383, 1.8158, -0.1778, -1.6137, -2.8270, -0.7473, -1.2111, -0.8492, -0.0123, 4.4376, 2.7845, -1.8530, -2.4199, 1.0255, 1.0187, 0.2340, -7.0777, 1.7685, 0.2038, 0.3453, 4.3879, 0.8334, 1.6510, 0.5630, -4.8142, 1.6344, 0.9091, -1.7864, -0.2434, 0.2208, -2.2463, -0.2130, 0.3011, -1.1533, 0.8240, 3.7837, 0.9226, 1.7746, 0.3289, -2.9486, 0.9123, 2.5809, -0.3828, -3.4875, 3.1742, 1.7787, 1.3277, 3.4460, -6.9343, -0.9153, 1.5912, 3.9272, 4.9742, -0.1913, -4.5837, -1.1200, -2.2561, 1.9209, 1.4090, -0.0935, -0.0269, -3.2420, 2.7441, 1.5252, -3.8591, 0.2098, -2.1438, -0.8087, -2.5660, -2.5496, 4.1721, 1.7778, 4.7410, -1.8366, 3.0977, 0.6357};

		final double delta = 1e-5;
		ArrayAssert.assertEquals(v1, e1, delta);
		ArrayAssert.assertEquals(v2, e2, delta);
		ArrayAssert.assertEquals(v3, e3, delta);


	}

	@Test
	public void testDataStorage() throws Exception {
		assertEquals(fvm.getNouns().size(), 2);
		assertEquals(fvm.getAdjs().size(), 4);
		assertEquals(fvm.getPairs().size(), 4);
		assertEquals(fvm.getAllTerms().size(), 10);
	}


	/*
	* Standard partition into training and testing (50/50)
	* */
	@Test
	public void testPartitionData1() throws Exception {
		fvm.newPartitionData(2, 0, true);
		assertEquals("Incorrect number of partitions created", 2, fvm.foldPartitions.size());
		testPartitionsTrainTestOverlap(false);
		testIfTrainDataOverMultiplePartitionsOverlaps();
	}

	/*
		* Test if partitioning with a different/same random seed produces different/same results
		* */
	@Test
	public void testPartitionData2() throws Exception {
		int[] seeds = new int[] {0, 1, 2, 3};
		Cloner cloner = new Cloner();
		for (int i = 0; i < seeds.length; i++) {
			fvm.newPartitionData(2, seeds[i], true);
			Map<Integer, Partition> firstAttempt = cloner.deepClone(fvm.foldPartitions);

			fvm.newPartitionData(2, seeds[i], true);
			Map<Integer, Partition> secondAttempt = cloner.deepClone(fvm.foldPartitions);
			for (Integer key : secondAttempt.keySet()) {
				Partition p1 = firstAttempt.get(key);
				Partition p2 = secondAttempt.get(key);
				assertEquals(CollectionUtils.intersection(p1.getTrainingData(), p2.getTrainingData()).size(), p1.getTrainingData().size());
				assertEquals(CollectionUtils.intersection(p1.getTestingData(), p2.getTestingData()).size(), p1.getTestingData().size());
			}

		}
	}


	@Test
	public void testPartitionEquals() throws Exception {
		SortedSet<String> set1 = new TreeSet<String>(Arrays.asList("w1 w1", "w1 w2", "w2 w1", "w2 w2"));
		SortedSet<String> set1a = new TreeSet<String>(Arrays.asList("w1 w1", "w2 w2"));
		SortedSet<String> set2 = new TreeSet<String>(Arrays.asList("w1 w1", "w1 w2", "w2 w1", "w2 w2"));
		SortedSet<String> set2a = new TreeSet<String>(Arrays.asList("w1 w1", "w2 w2"));

		Partition p1 = new Partition(set1, Collections.<String>emptySet(), 1, 1);
		Partition p2 = new Partition(set2, Collections.<String>emptySet(), 1, 1);
		assertEquals(p1, p2);
		assertEquals(p1.hashCode(), p2.hashCode());


		Partition p1a = new Partition(set1, set2a, 3, 14);
		Partition p2a = new Partition(set2, set1a, 5, 18);
		assertEquals(p1a, p2a);
		assertEquals(p1a.hashCode(), p2a.hashCode());

	}

	/**
	 * Checks if adjCentricPartition's training and testing data overlap
	 *
	 * @param overlap if true, the two sets are required to overlap, otherwise required NOT to overlap
	 */
	private void testPartitionsTrainTestOverlap(boolean overlap) {
		log.info("test if Partition's Train and Test Sets Overlap");
		for (Partition p : fvm.foldPartitions.values()) {
			for (String bigram : p.getTrainingData()) {
				assertEquals(p.getTestingData().contains(bigram), overlap);
			}
			for (String bigram : p.getTestingData()) {
				assertEquals(p.getTrainingData().contains(bigram), overlap);
			}
		}
	}

	private void testIfTrainDataOverMultiplePartitionsOverlaps() {
		log.info("test If Train Data Over Multiple Partitions Overlaps");
		final Collection<Partition> partitions = fvm.foldPartitions.values();
		Cloner c = new Cloner();
		for (Partition thisPartition : partitions) {
			Collection<Partition> others = c.deepClone(partitions);
			others.remove(thisPartition);

			for (Partition other : others) {
				assertFalse(CollectionUtils.intersection(other.getTrainingData(), thisPartition.getTrainingData()).size() > 0);
				assertFalse(CollectionUtils.intersection(other.getTestingData(), thisPartition.getTestingData()).size() > 0);
			}
		}
	}
}
